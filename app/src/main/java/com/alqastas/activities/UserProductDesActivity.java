package com.alqastas.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.ColorsAdapter;
import com.alqastas.adapters.CommentsAdapter;
import com.alqastas.adapters.CustomPagerAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.DefaultResponse;
import com.alqastas.models.responses.ItemColor;
import com.alqastas.models.responses.ItemImage;
import com.alqastas.models.responses.ItemReview;
import com.alqastas.models.responses.ItemSize;
import com.alqastas.models.responses.ProductDetails;
import com.alqastas.models.responses.ReviewsResponse;
import com.alqastas.utils.Const;
import com.alqastas.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProductDesActivity extends ParentActivity {

    private TextView price;
    private TextView productTitle;
    private TextView sellerName;
    private TextView warranty;
    private TextView description;
    private TextView qty;
    private TextView tvComments;
    private TextView toolbarTitle;
//    private Spinner sizeSpinner;

    private AppCompatRatingBar ratingBar;
    private Button addToCart;
    private ImageButton back;

    private LinearLayout layQty;
    private LinearLayout laySize;
    private LinearLayout layComments;
    private LinearLayout layColors;

    private ImageView plusQty;
    private ImageView minusQty;

    private RecyclerView colorRecycler;
    private RecyclerView commentsRecycler;

    private ViewPager viewPager;

    private CustomPagerAdapter mCustomPagerAdapter;
    private CommentsAdapter commentsAdapter;
    private ColorsAdapter colorsAdapter;

    private List<ItemSize> itemSizeList;
    private long productId;
    private int currentQty = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_product_des);

        initFirstValues();
    }

    private void initFirstValues() {

        productId = getIntent().getLongExtra(Const.PRODUCT_ID, 0);

        price = findViewById(R.id.price);
        productTitle = findViewById(R.id.productTitle);
        sellerName = findViewById(R.id.sellerName);
        warranty = findViewById(R.id.warranty);
        description = findViewById(R.id.description);
        qty = findViewById(R.id.qty);
        tvComments = findViewById(R.id.tvComments);
        toolbarTitle = findViewById(R.id.toolbarTitle);
//        sizeSpinner = findViewById(R.id.sizeSpinner);
        ratingBar = findViewById(R.id.rateBar);

        layQty = findViewById(R.id.layQty);
        laySize = findViewById(R.id.laySize);
        layComments = findViewById(R.id.layComments);
        layColors = findViewById(R.id.layColor);

        plusQty = findViewById(R.id.plusQty);
        plusQty.setOnClickListener(this);
        minusQty = findViewById(R.id.minusQty);
        minusQty.setOnClickListener(this);
        addToCart = findViewById(R.id.addToCart);
        addToCart.setOnClickListener(this);
        back = findViewById(R.id.back);
        back.setOnClickListener(this);

        colorRecycler = findViewById(R.id.colorRecycler);
        commentsRecycler = findViewById(R.id.commentsRecycler);
        viewPager = findViewById(R.id.viewPager);

        toolbarTitle.setText(getString(R.string.productDetails));
        itemSizeList = new ArrayList<>();
        getProductDetails(productId);
        getReviewsCall(productId);
    }

    private void setProductDetails(ProductDetails product) {

        if (product.getItemReviews() != null) {
            setCommentsRecycler(product.getItemReviews());
        } else {
            tvComments.setText(getString(R.string.noComments));
        }
        if (product.getInsuranceEndAt() != null) {
            warranty.setText(getString(R.string.Insurance_end_at) + " " + product.getInsuranceEndAt());
        }
        price.setText(product.getItemPrice() + " " + getString(R.string.rs));
        productTitle.setText(product.getItemNameAR());
        sellerName.setText(product.getSellerName());
        description.setText(product.getItemDescAr());

        logE("The product rating : " + product.getRating());
        if (product.getRating() != null) {
            ratingBar.setRating(product.getRating());
        } else {
            ratingBar.setRating(0);
        }
        qty.setText("0");

//        setProductColors(product.getItemColors());

        setPhotosPager(product.getItemImages());

        itemSizeList = product.getItemSizes();
//        setSizeSpinner();

        if (product.getItemSizes() == null || product.getItemSizes().size() == 0) {
            laySize.setVisibility(View.GONE);
        }
        if (product.getItemColors() == null || product.getItemColors().size() == 0) {
            layColors.setVisibility(View.GONE);
        }
    }

//    private void setSizeSpinner() {
//
//        ItemSize itemSize = new ItemSize();
//        itemSize.setId((long) 0);
//        itemSize.setSizeDesc(getString(R.string.size));
//        itemSize.setSizeDesc(getString(R.string.sizeAr));
//        itemSizeList.add(0, itemSize);
//
//        ArrayAdapter<ItemSize> dataAdapter = new ArrayAdapter<>
//                (activity, android.R.layout.simple_spinner_item, itemSizeList);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        sizeSpinner.setAdapter(dataAdapter);
//    }
//
//    private int sizeId() {
//        int id = 0;
//        sizeSpinner.setOnItemClickListener((parent, view, position, id1) ->
//                id1 = itemSizeList.get(position).getSizeId());
//        return id;
//    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.plusQty:

                currentQty = Integer.parseInt(Utils.getText(qty));
                qty.setText(String.valueOf(currentQty + 1));

                break;
            case R.id.minusQty:
                currentQty = Integer.parseInt(Utils.getText(qty));
                if (currentQty > 0) {
                    qty.setText(String.valueOf(currentQty - 1));
                }

                break;

            case R.id.addToCart:

                if (!Utils.getText(qty).equals("0")) {
                    postItemToCart();
                } else {
                    Utils.showLongToast(activity, R.string.must_enter_item_qty);
                }
                break;
            case R.id.back:
                onBackPressed();
                break;
        }
    }

    private void setPhotosPager(List<ItemImage> images) {

        mCustomPagerAdapter = new CustomPagerAdapter(activity, images);
        viewPager.setAdapter(mCustomPagerAdapter);
    }

    private void setCommentsRecycler(List<ItemReview> commentsList) {

        commentsRecycler.setLayoutManager(new LinearLayoutManager(activity));
        commentsAdapter = new CommentsAdapter(activity, commentsList, R.layout.item_comments);
        commentsRecycler.setAdapter(commentsAdapter);
    }

    private void setProductColors(List<ItemColor> colorList) {

        colorRecycler.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        colorsAdapter = new ColorsAdapter(activity, colorList, R.layout.item_color);
        colorRecycler.setAdapter(colorsAdapter);
    }

    private void getProductDetails(long productId) {
        Call<ProductDetails> call = serviceAPI.getProductDetails(productId,
                Utils.getCachedString(activity, Const.USER_ID, ""));

        call.enqueue(new Callback<ProductDetails>() {
            @Override
            public void onResponse(@NonNull Call<ProductDetails> call, @NonNull Response<ProductDetails> response) {
                if (response.isSuccessful()) {

                    setProductDetails(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductDetails> call, @NonNull Throwable t) {

                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void postItemToCart() {

        Call<List<DefaultResponse>> call = serviceAPI.postUserCart(productId, Integer.parseInt(Utils.getText(qty)),
                Utils.getCachedString(activity, Const.USER_ID, ""));

        call.enqueue(new Callback<List<DefaultResponse>>() {
            @Override
            public void onResponse(Call<List<DefaultResponse>> call, Response<List<DefaultResponse>> response) {

                if (response.isSuccessful()) {

                    Utils.showLongToast(activity, getString(R.string.itemAddedToCart));
                    onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<List<DefaultResponse>> call, Throwable t) {
                RetrofitClientInstance.checkFailureType(t ,activity);
            }
        });
    }

    private void getReviewsCall(long productId) {

        Call<ReviewsResponse> call = serviceAPI.getProductReviews(productId, "1", "15");
        call.enqueue(new Callback<ReviewsResponse>() {
            @Override
            public void onResponse(Call<ReviewsResponse> call, Response<ReviewsResponse> response) {

                if (response.isSuccessful() && response.body().getCommentsList() != null) {
                    setCommentsRecycler(response.body().getCommentsList());
                }
            }

            @Override
            public void onFailure(Call<ReviewsResponse> call, Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }
}
