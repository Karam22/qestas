package com.alqastas.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.alqastas.R;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.connections.ServiceAPI;
import com.alqastas.utils.DialogUtils;
import com.alqastas.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ParentActivity extends AppCompatActivity implements View.OnClickListener {
    // used to hold connection handlers that should be cancelled when destroyed
    protected AppCompatActivity activity;
    protected ProgressDialog progressDialog;
    private FrameLayout rootView;
    public ImageButton cart;
    ImageButton back;
    private Toolbar toolbar;
    private int menuId;
    private boolean enableBack;
    private int iconResId;
    public FusedLocationProviderClient mFusedLocationProvider;

    public ServiceAPI serviceAPI;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        activity = this;
        rootView = findViewById(android.R.id.content);
        mFusedLocationProvider = LocationServices.getFusedLocationProviderClient(activity);

        serviceAPI = RetrofitClientInstance.getRetrofitInstance().create(ServiceAPI.class);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        try {
            cart = findViewById(R.id.cart);
            cart.setOnClickListener(this);
            back = findViewById(R.id.back);
            back.setOnClickListener(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        if (activity instanceof UserCartActivity || activity instanceof ChangePasswordActivity
                || activity instanceof AddHarajProduct) {
            cart.setVisibility(View.GONE);
        } else if (activity instanceof HarajProductsActivity) {
            cart.setImageResource(R.drawable.plus);
        }

    }

    public void logE(String msg) {
        Utils.logE(msg);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.cart:

                if (activity instanceof HarajProductsActivity) {
                    // add haraj activity intent
                    startActivity(new Intent(activity, AddHarajProduct.class));
                } else {
                    startActivity(new Intent(activity, UserCartActivity.class));
                }
                break;
            case R.id.back:
                onBackPressed();
                break;
        }
    }

    public void showProgressDialog() {
        if (progressDialog != null) {
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
        } else {
            progressDialog = DialogUtils.showProgressDialog(this, R.string.please_wait_dotted);
        }
    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public int getResColor(int id) {
        return getResources().getColor(id);
    }

    public void loadFragment(int container, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(container, fragment)
                .commitAllowingStateLoss();
    }

    public void hideKeyboard() {
        if (rootView != null) {
            Utils.hideKeyboard(rootView);
        }
    }


    public void createOptionsMenu(int menuId) {
        this.menuId = menuId;
        invalidateOptionsMenu();
    }

    public void removeOptionsMenu() {
        menuId = 0;
        invalidateOptionsMenu();
    }


    public void setToolbarIcon(int iconResId) {
        this.iconResId = iconResId;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menuId != 0) {
            getMenuInflater().inflate(menuId, menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home && enableBack) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public boolean hasToolbar() {
        return toolbar != null;
    }
}