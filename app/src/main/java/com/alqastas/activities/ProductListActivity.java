package com.alqastas.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.ProductsAdapter;
import com.alqastas.adapters.SubCategoryAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.LoadingProductsListener;
import com.alqastas.models.responses.Product;
import com.alqastas.models.responses.ProductsResponse;
import com.alqastas.models.responses.SubCategoriesResponse;
import com.alqastas.models.responses.SubCategory;
import com.alqastas.utils.Const;
import com.alqastas.utils.PaginationScrollListener;
import com.alqastas.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListActivity extends ParentActivity {

    private TextView toolbarTitle;
    private ImageButton back;
    private View line;
    private RecyclerView subCategories;
    private RecyclerView productRecycler;

    private SubCategoryAdapter subCategoryAdapter;
    private ProductsAdapter productsAdapter;

    private String categoryId;
    private String categoryName = "";
    private String subCategoryId = "";
    private int currentPage = 0;
    private int TOTAL_PAGES = 100;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private List<Product> productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        initFirstValues();
    }

    private void initFirstValues() {

        categoryId = String.valueOf(getIntent().getLongExtra(Const.CATEGORY_ID, 0));
        categoryName = getIntent().getStringExtra(Const.CATEGORY_NAME);

        subCategories = findViewById(R.id.subCategories);
        productRecycler = findViewById(R.id.products);
        toolbarTitle = findViewById(R.id.toolbarTitle);
        line = findViewById(R.id.line);
        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        productList = new ArrayList<>();

        toolbarTitle.setText(categoryName);
        setProductsList(productList);
        subCategoriesCall();
    }

    private void setSubCategoriesList(List<SubCategory> subCategoriesList) {

        subCategories.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        subCategoryAdapter = new SubCategoryAdapter(activity, subCategoriesList, R.layout.item_sub_category);
        subCategories.setAdapter(subCategoryAdapter);
    }

    private void setProductsList(List<Product> listProducts) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        productRecycler.setLayoutManager(linearLayoutManager);
        productsAdapter = new ProductsAdapter(activity, listProducts, R.layout.item_product);
        productsAdapter.addAll(listProducts);
        productRecycler.setAdapter(productsAdapter);
        if (currentPage <= TOTAL_PAGES) productsAdapter.addLoadingFooter();
        else isLastPage = true;


        productRecycler.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNewPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    private void loadNewPage() {
        listProductsCall(subCategoryId, String.valueOf(currentPage));
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v.getId() == R.id.back) {
            onBackPressed();
        }
    }

    @Subscribe
    public void onEvent(LoadingProductsListener listener) {

        subCategoryId = String.valueOf(listener.getSubCategoryId());
        listProductsCall(subCategoryId, String.valueOf(currentPage));
    }

    private void subCategoriesCall() {

        Call<SubCategoriesResponse> call = serviceAPI.getSubCategories(categoryId, "1", "30");
        call.enqueue(new Callback<SubCategoriesResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubCategoriesResponse> call, @NonNull Response<SubCategoriesResponse> response) {

                if (response.isSuccessful() && response.body().getSubCategoryList() != null
                        && response.body().getSubCategoryList().size() > 0) {

                    setSubCategoriesList(response.body().getSubCategoryList());
                } else {

                    listProductsCall("", String.valueOf(currentPage));
                    subCategories.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubCategoriesResponse> call, @NonNull Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void listProductsCall(String subCategoryId, String pageNo) {

        Call<ProductsResponse> call = serviceAPI.getProducts(categoryId, subCategoryId
                , Utils.getCachedString(activity, Const.USER_ID, ""), pageNo, "30");

        call.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProductsResponse> call, @NonNull Response<ProductsResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getProductList().size() > 0) {

                        isLoading = false;
                        productsAdapter.addAll(response.body().getProductList());

//                        if (currentPage != TOTAL_PAGES) productsAdapter.addLoadingFooter();
//                        else isLastPage = true;

                    } else {
                        productsAdapter.removeLoadingFooter();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductsResponse> call, @NonNull Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });

    }
}
