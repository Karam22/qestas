package com.alqastas.activities;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.utils.BitmapUtils;
import com.alqastas.utils.Utils;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

public class AddHarajProduct extends PicPickerActivity {

    private TextInputEditText productName;
    private TextInputEditText productPrice;
    private TextInputEditText productDesc;
    private TextInputEditText address;
    private TextInputEditText email;
    private TextInputEditText mobileNumber;
    private ImageButton pic1;
    private ImageButton pic2;
    private ImageButton pic3;
    private Button save;
    private TextView toolbarTitle;

    private String image1 = "";
    private String image2 = "";
    private String image3 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_haraj_product);

        initFirstValues();
    }

    private void initFirstValues() {

        toolbarTitle = findViewById(R.id.toolbarTitle);
        productName = findViewById(R.id.productName);
        productPrice = findViewById(R.id.productPrice);
        productDesc = findViewById(R.id.productDescription);
        address = findViewById(R.id.address);
        email = findViewById(R.id.email);
        mobileNumber = findViewById(R.id.mobileNumber);
        pic1 = findViewById(R.id.pic1);
        pic1.setOnClickListener(this);
        pic2 = findViewById(R.id.pic2);
        pic2.setOnClickListener(this);
        pic3 = findViewById(R.id.pic3);
        pic3.setOnClickListener(this);
        save = findViewById(R.id.save);
        save.setOnClickListener(this);

        toolbarTitle.setText(getString(R.string.add_to_haraj));
    }

    @Override
    public void onImageReady(int requestCode, File image) {

        switch (requestCode) {

            case 1:
                image1 = BitmapUtils.getStringImage(image);
                Picasso.get().load(image).fit().memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(pic1);
                break;

            case 2:
                image2 = BitmapUtils.getStringImage(image);
                Picasso.get().load(image).fit().memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(pic2);

                break;

            case 3:
                image3 = BitmapUtils.getStringImage(image);
                Picasso.get().load(image).fit().memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(pic3);

                break;
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.pic1:

                captureFromCamera(1, true);
                break;
            case R.id.pic2:
                captureFromCamera(2, true);

                break;
            case R.id.pic3:
                captureFromCamera(3, true);
                break;

            case R.id.save:

                if (dataIsValid()){
                    // save haraj product
                }
                break;
        }
    }

    private boolean dataIsValid() {
        boolean valid = false;
        if (Utils.isEmpty(productName)) {
            productName.setError(getString(R.string.filed_required));
            productName.requestFocus();
        } else if (Utils.isEmpty(productPrice)) {
            productPrice.setError(getString(R.string.filed_required));
            productPrice.requestFocus();
        } else if (Utils.isEmpty(productDesc)) {
            productDesc.setError(getString(R.string.filed_required));
            productDesc.requestFocus();
        } else if (Utils.isEmpty(address)) {
            address.setError(getString(R.string.filed_required));
            address.requestFocus();
        } else if (Utils.isEmpty(email)) {
            email.setError(getString(R.string.filed_required));
            email.requestFocus();
        } else if (Utils.isEmpty(mobileNumber)) {
            mobileNumber.setError(getString(R.string.filed_required));
            mobileNumber.requestFocus();
        } else if (image1.equals("") || image2.equals("") || image3.equals("")) {

            Utils.showLongToast(activity, R.string.must_pic_3_photos);
        } else {
            valid = true;
        }
        return valid;
    }
}
