package com.alqastas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.DefaultResponse;
import com.alqastas.utils.Const;
import com.alqastas.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressActivity extends ParentActivity implements View.OnClickListener {

    private TextInputEditText streetName;
    private TextInputEditText apartmentNumber;
    private TextInputEditText areaName;
    private TextInputEditText city;

    private Button btnSave;
    private ImageButton back;
    private TextView toolbarTitle;

    private boolean dataValid = false;
    private String destination = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        streetName = findViewById(R.id.streetName);
        apartmentNumber = findViewById(R.id.apartmentNumber);
        areaName = findViewById(R.id.areaName);
        city = findViewById(R.id.city);
        toolbarTitle = findViewById(R.id.toolbarTitle);
        btnSave = findViewById(R.id.save);
        btnSave.setOnClickListener(this);
        back = findViewById(R.id.back);
        back.setOnClickListener(this);

        toolbarTitle.setText(getString(R.string.add_new_address));
        destination = getIntent().getStringExtra(Const.DESTINATION);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.save:

                if (validateAddress()) {

                    String address = Utils.getText(streetName) + " , " + Utils.getText(apartmentNumber)
                            + " , " + Utils.getText(areaName) + " , " + Utils.getText(city);
                    addAddressCall(address);
                }
                break;
            case R.id.back:
                onBackPressed();
                break;
        }
    }

    private void addAddressCall(String userAddress) {

        Call<DefaultResponse> addressCall = serviceAPI.AddUserAddress(
                Utils.getCachedString(activity, Const.USER_ID, ""), userAddress);
        addressCall.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {

                if (response.isSuccessful()) {

                    Utils.showShortToast(activity, R.string.address_saved);
                    streetName.setText("");
                    apartmentNumber.setText("");
                    areaName.setText("");
                    city.setText("");
                    onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {

                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (destination != null && destination.equals(Const.CHOOSE_ADDRESS)) {
            startActivity(new Intent(activity, ChooseAddressActivity.class));
            finish();
        } else if (destination != null && destination.equals(Const.MY_ADDRESSES)) {
            startActivity(new Intent(activity, MyAddressActivity.class));
            finish();
        } else {
            onBackPressed();
        }
    }

    private boolean validateAddress() {

        if (Utils.isEmpty(streetName)) {
            setEditError(streetName);

        } else if (Utils.isEmpty(apartmentNumber)) {
            setEditError(apartmentNumber);
        } else if (Utils.isEmpty(areaName)) {
            setEditError(areaName);
        } else if (Utils.isEmpty(city)) {
            setEditError(city);
        } else {
            dataValid = true;
        }
        return dataValid;
    }

    private void setEditError(EditText view) {

        view.setError(getString(R.string.filed_required));
        view.requestFocus();
        dataValid = false;
    }
}
