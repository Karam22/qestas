package com.alqastas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.UserCartAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.DeleteCartListener;
import com.alqastas.models.UpdateCartListener;
import com.alqastas.models.responses.Cart;
import com.alqastas.models.responses.DefaultResponse;
import com.alqastas.utils.Const;
import com.alqastas.utils.Utils;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserCartActivity extends ParentActivity {

    private RecyclerView recyclerView;
    private TextView totalPrice;
    private TextView error;
    private TextView toolbarTitle;
    private Button btnCompleteOrder;

    private UserCartAdapter cartAdapter;
    private List<Cart> cartList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_cart);

        setInitValues();
    }

    private void setInitValues() {

        recyclerView = findViewById(R.id.recyclerView);
        totalPrice = findViewById(R.id.totalPrice);
        toolbarTitle = findViewById(R.id.toolbarTitle);

        error = findViewById(R.id.error);
        btnCompleteOrder = findViewById(R.id.btnComplete);
        btnCompleteOrder.setOnClickListener(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        toolbarTitle.setText(getString(R.string.cart));
        cartList = new ArrayList<>();
        getUserCart();
    }

    @Subscribe
    public void onEvent(UpdateCartListener listener) {

        updateUserCart(listener.getCartId(), listener.getQty(), listener.getPosition());
    }

    @Subscribe
    public void onEvent(DeleteCartListener listener) {
        deleteUserCart((int) listener.getCartId(), listener.getPosition());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.btnComplete) {
            startActivity(new Intent(activity, ChooseAddressActivity.class));
        }
    }

    private void calculateTotalPrice() {

        float price = 0;
        for (int i = 0; i < cartList.size(); i++) {
            price = price + (Float.parseFloat(cartList.get(i).getPrice()) * cartList.get(i).getQty());
        }
        totalPrice.setText(String.format("%s %s", String.valueOf(price), getString(R.string.rs)));
    }

    private void setCartRecycler() {

        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        cartAdapter = new UserCartAdapter(activity, cartList, R.layout.item_cart);
        recyclerView.setAdapter(cartAdapter);
    }

    private void getUserCart() {

        Call<List<Cart>> call = serviceAPI.getCartItems(Utils.getCachedString(activity,
                Const.USER_ID, ""));
        call.enqueue(new Callback<List<Cart>>() {
            @Override
            public void onResponse(@NonNull Call<List<Cart>> call, @NonNull Response<List<Cart>> response) {

                if (response.isSuccessful() && response.body() != null && response.body().size() > 0) {

                    cartList = response.body();
                    setCartRecycler();
                    calculateTotalPrice();

                } else {
                    handleEmptyRecycler();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Cart>> call, @NonNull Throwable t) {

                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void updateUserCart(long cartItemId, int qty, int itemPosition) {

        Call<List<DefaultResponse>> call = serviceAPI.updateUserCart(cartItemId, qty);
        call.enqueue(new Callback<List<DefaultResponse>>() {
            @Override
            public void onResponse(Call<List<DefaultResponse>> call, Response<List<DefaultResponse>> response) {

                if (response.isSuccessful()) {

                    cartList.get(itemPosition).setQty(qty);
                    cartAdapter.notifyItemChanged(itemPosition);
                    calculateTotalPrice();
                } else {

                    Utils.showShortToast(activity, getString(R.string.something_wrong_happened));
                }
            }

            @Override
            public void onFailure(Call<List<DefaultResponse>> call, Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void deleteUserCart(long cartItemId, int position) {

        Call<List<DefaultResponse>> call1 = serviceAPI.deleteUserCart(cartItemId);
        call1.enqueue(new Callback<List<DefaultResponse>>() {
            @Override
            public void onResponse(Call<List<DefaultResponse>> call, Response<List<DefaultResponse>> response) {
                if (response.isSuccessful()) {

                    cartList.remove(position);
                    cartAdapter.notifyItemRemoved(position);
                    calculateTotalPrice();
                } else {

                    Utils.showShortToast(activity, getString(R.string.something_wrong_happened));
                }
            }

            @Override
            public void onFailure(Call<List<DefaultResponse>> call, Throwable t) {

            }
        });
    }

    private void handleEmptyRecycler() {

        recyclerView.setVisibility(View.GONE);
        btnCompleteOrder.setVisibility(View.GONE);
        error.setVisibility(View.VISIBLE);
        error.setText(getString(R.string.cart_empty_message));
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }
}
