package com.alqastas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.ChoosesAddressAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.Address;
import com.alqastas.utils.Const;
import com.alqastas.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseAddressActivity extends ParentActivity {

    private Button completeOrder;
    private Button addAddress;
    private RecyclerView recyclerView;
    private ChoosesAddressAdapter addressAdapter;
    private TextView error;
    private TextView toolbarTitle;
    private ImageButton back;

    private List<Address> addressList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_address);

        completeOrder = findViewById(R.id.btnComplete);
        completeOrder.setOnClickListener(this);
        addAddress = findViewById(R.id.addAddress);
        addAddress.setOnClickListener(this);
        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        recyclerView = findViewById(R.id.recyclerView);
        error = findViewById(R.id.error);
        toolbarTitle = findViewById(R.id.toolbarTitle);

        toolbarTitle.setText(getString(R.string.choose_address));

        addressList = new ArrayList<>();
        getUserAddress();
    }

    private void setAddressesRecycler() {

        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        addressAdapter = new ChoosesAddressAdapter(activity, addressList, R.layout.item_choose_address);
        recyclerView.setAdapter(addressAdapter);
    }

    private void getUserAddress() {

        Call<List<Address>> call = serviceAPI.getUserAddresses(Utils.getCachedString(activity,
                Const.USER_ID, ""));
        call.enqueue(new Callback<List<Address>>() {
            @Override
            public void onResponse(@NonNull Call<List<Address>> call, @NonNull Response<List<Address>> response) {

                if (response.isSuccessful() && response.body() != null && response.body().size() > 0) {

                    addressList = response.body();
                    setAddressesRecycler();
                    error.setVisibility(View.GONE);
                    completeOrder.setVisibility(View.VISIBLE);
                } else {

                    handleEmptyError();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Address>> call, @NonNull Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void handleEmptyError() {

        recyclerView.setVisibility(View.GONE);
        completeOrder.setVisibility(View.GONE);
        error.setText(getString(R.string.no_registered_addresses));
        error.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.btnComplete:
                startActivity(new Intent(activity, OrderDetailsActivity.class)
                        .putExtra(Const.ADDRESS, addressAdapter.getCheckedItem()));
                break;

            case R.id.addAddress:

                startActivity(new Intent(activity, AddAddressActivity.class)
                .putExtra(Const.DESTINATION , Const.CHOOSE_ADDRESS));
                finish();

                break;

            case R.id.back:
                onBackPressed();

                break;
        }
    }
}
