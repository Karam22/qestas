package com.alqastas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.UserResponse;
import com.alqastas.utils.Const;
import com.alqastas.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ParentActivity implements View.OnClickListener {

    private TextInputEditText mobileNumber;
    private TextInputEditText password;
    private TextView tvForgetPassword;

    private Button btnLogin;
    private Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mobileNumber = findViewById(R.id.mobileNumber);
        password = findViewById(R.id.password);
        tvForgetPassword = findViewById(R.id.forgetPassword);
        btnLogin = findViewById(R.id.login);
        btnSignUp = findViewById(R.id.signUp);

        btnSignUp.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        Utils.hideKeyboard(activity);
    }

    private void loginValidate() {

        if (Utils.isEmpty(mobileNumber)) {

            mobileNumber.setError(getString(R.string.enter_mobile_number));
        } else if (Utils.isEmpty(password)) {
            password.setError(getString(R.string.enter_password));
        } else {
            loginCall(Utils.getText(mobileNumber), Utils.getText(password));
        }
    }

    private void showForgetPassword() {

    }

    private void loginCall(String mobileNumber, String password) {

        showProgressDialog();

        Call<UserResponse> userCall = serviceAPI.loginUser(mobileNumber, password, "password");
        userCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                hideProgressDialog();
                if (response.isSuccessful()) {

                    logE("The token : " + response.body().getToken());
                    Utils.cacheString(activity, Const.USER_TOKEN, response.body().getToken());
                    Utils.cacheString(activity, Const.USER_ID, response.body().getUserId());
                    Utils.cacheString(activity, Const.USER_MOBILE, mobileNumber);
                    Utils.showShortToast(activity, getString(R.string.welcome) + " "
                            + response.body().getDisplayName());
                    startActivity(new Intent(activity, HomeActivity.class));
                    finish();
                } else {
                    Utils.showShortToast(activity, R.string.mobile_passowrd_not_correct);
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                hideProgressDialog();
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.login:

                loginValidate();

                break;
            case R.id.signUp:
                startActivity(new Intent(activity, SignUpActivity.class));
                finish();
                break;
        }
    }
}
