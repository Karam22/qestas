package com.alqastas.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.alqastas.R;
import com.alqastas.adapters.HomeListAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.AnnoncmentResponse;
import com.alqastas.models.responses.DefaultResponse;
import com.alqastas.models.responses.HomeCategories;
import com.alqastas.models.responses.HomeResponse;
import com.alqastas.utils.Const;
import com.alqastas.utils.DialogUtils;
import com.alqastas.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends NavigationActivity  {

    private RecyclerView recyclerView;
    private ViewFlipper viewPager;

    private HomeListAdapter adapter;
    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_home, null, false);
        drawer_menu.addView(contentView, 0);

        initFirstViews();
        getHomeResponse();
    }

    private void initFirstViews() {

        recyclerView = findViewById(R.id.recyclerView);
        viewPager = findViewById(R.id.viewPager);

    }

    private void setFlipperImage(String imagePath) {

        ImageView imageView = new ImageView(this);
        if (imagePath != null) {
            Picasso.get().load(imagePath).fit().into(imageView);
        } else {
            imageView.setImageResource(R.drawable.logo);
        }
        viewPager.addView(imageView);
        viewPager.setInAnimation(this, android.R.anim.slide_in_left);
        viewPager.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    private void getHomeResponse() {

        showProgressDialog();
        Call<HomeResponse> homeCall = serviceAPI.getHomeResponse(Utils.getCachedString(this,
                Const.USER_ID, ""));

        homeCall.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {

                if (response.isSuccessful()) {
                    handleHomeLists(response.body().getHomeCategoriesList());
                    for (AnnoncmentResponse announcement : response.body().getAnnoncmentResponseList()) {
                        setFlipperImage(announcement.getAnnoncmentImagePath());
                    }
                    handleAdsClicks(response.body().getAnnoncmentResponseList());
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                hideProgressDialog();
                RetrofitClientInstance.checkFailureType(t, HomeActivity.this);
            }
        });
    }

//    private void getAnnouncementList() {
//
//        Call<List<AnnoncmentResponse>> call = serviceAPI.getAnnouncement();
//        call.enqueue(new Callback<List<AnnoncmentResponse>>() {
//            @Override
//            public void onResponse(Call<List<AnnoncmentResponse>> call, Response<List<AnnoncmentResponse>> response) {
//
//                if (response.isSuccessful() && response.body() != null) {
//
//
//                    getHomeResponse();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<AnnoncmentResponse>> call, Throwable t) {
//
//                RetrofitClientInstance.checkFailureType(t, HomeActivity.this);
//            }
//        });
//    }

    private void handleAdsClicks(List<AnnoncmentResponse> announcementList) {

        viewPager.setOnClickListener(v -> {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                    announcementList.get(viewPager.getDisplayedChild()).getDestination()));
            startActivity(browserIntent);

            postAnnouncementClick(announcementList.get(viewPager.getDisplayedChild()).getId());
        });
    }

    private void handleHomeLists(List<HomeCategories> homeList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new HomeListAdapter(this, homeList, R.layout.item_home_list);
        recyclerView.setAdapter(adapter);
    }

    private void postAnnouncementClick(long announcementId) {

        Call<DefaultResponse> call = serviceAPI.postAnnouncementClick(announcementId,
                Utils.getCachedString(this, Const.USER_ID, ""));
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {

            }
        });
    }

    public void showProgressDialog() {
        if (progressDialog != null) {
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
        } else {
            progressDialog = DialogUtils.showProgressDialog(this, R.string.please_wait_dotted);
        }
    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}
