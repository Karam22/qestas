package com.alqastas.activities;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.utils.Utils;

public class ChangePasswordActivity extends ParentActivity {

    private TextView toolbarTitle;
    private TextInputEditText oldPassword;
    private TextInputEditText newPassword;
    private TextInputEditText confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);

        toolbarTitle.setText(getString(R.string.change_password));

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()){

            case R.id.send:

                if (isValid()){

                }
                break;
        }
    }

    private boolean isValid(){
        boolean valid = false;

        if (Utils.isEmpty(oldPassword)){
            oldPassword.setError(getString(R.string.filed_required));
            oldPassword.requestFocus();
        }else if (Utils.isEmpty(newPassword)){
            newPassword.setError(getString(R.string.filed_required));
            newPassword.requestFocus();
        }else if (Utils.isEmpty(confirmPassword)){
            confirmPassword.setError(getString(R.string.filed_required));
            confirmPassword.requestFocus();
        }else if (!Utils.getText(newPassword).equals(Utils.getText(confirmPassword))){
            confirmPassword.setError(getString(R.string.password_not_equal));
            confirmPassword.requestFocus();
        }else {
            valid = true;
        }
        return valid;
    }
}
