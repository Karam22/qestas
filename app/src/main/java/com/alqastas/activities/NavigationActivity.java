package com.alqastas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;

import com.alqastas.R;
import com.alqastas.utils.Utils;

public class NavigationActivity extends ParentActivity {

    ImageButton ibMenu;
    ImageButton ibSearch;
    ImageButton ibAccount;
    ImageButton ibCart;

    DrawerLayout drawer_menu;
    NavigationView userNavigation;
    static int DRAWER_GRAVITY = Gravity.START;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_layout);

        initFirstViews();
        handleNavigationClicks();
    }

    private void initFirstViews() {

        ibMenu = findViewById(R.id.menu);
        ibSearch = findViewById(R.id.search);
        ibAccount = findViewById(R.id.myAccount);
        ibCart = findViewById(R.id.cart);
        drawer_menu = findViewById(R.id.drawer_menu);
        userNavigation = findViewById(R.id.userNavigation);

        ibMenu.setOnClickListener(this);
        ibSearch.setOnClickListener(this);
        ibAccount.setOnClickListener(this);
        ibCart.setOnClickListener(this);
    }

    private void handleNavigationClicks() {

        userNavigation.setNavigationItemSelectedListener(menuItem -> {

            switch (menuItem.getItemId()) {
                case R.id.favourite:
                    startActivity(new Intent(activity, FavouriteProductsActivity.class));
                    break;
                case R.id.categories:
                    startActivity(new Intent(activity, CategoryActivity.class));
                    break;
                case R.id.myOrders:
                    break;
                case R.id.myAddress:
                    startActivity(new Intent(activity, MyAddressActivity.class));
                    break;
                case R.id.balance:
                    Utils.showShortToast(activity, "تحت الانشاء");
                    break;
                case R.id.settings:
                    Utils.showShortToast(activity, "تحت الانشاء");
                    break;
                case R.id.changePassword:
                    break;
                case R.id.haraj:

                    startActivity(new Intent(activity, HarajProductsActivity.class));
                    break;
            }
            return false;
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.menu:

                if (drawer_menu.isDrawerOpen(DRAWER_GRAVITY)) {
                    closeMenuDrawer();
                } else {
                    drawer_menu.openDrawer(DRAWER_GRAVITY);
                }

                break;
            case R.id.search:
                startActivity(new Intent(activity, UserSearchActivity.class));

                break;
            case R.id.myAccount:
                break;
            case R.id.cart:
                startActivity(new Intent(activity, UserCartActivity.class));
                break;
        }
    }

    public void closeMenuDrawer() {
        drawer_menu.closeDrawer(DRAWER_GRAVITY);
    }

    @Override
    public void onBackPressed() {
        if (drawer_menu.isDrawerOpen(DRAWER_GRAVITY)) {
            closeMenuDrawer();
        } else {
            super.onBackPressed();
            finish();
        }
    }


}
