package com.alqastas.activities;

import android.os.Bundle;

import com.alqastas.R;

public class MyAccountActivity extends ParentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
    }
}
