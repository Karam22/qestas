package com.alqastas.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.ProductsAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.Product;
import com.alqastas.utils.Const;
import com.alqastas.utils.PaginationScrollListener;
import com.alqastas.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserSearchActivity extends ParentActivity implements TextView.OnEditorActionListener {

    private TextInputEditText searchView;
    private TextView nearest;
    private TextView lowestPrice;
    private TextView toolbarTitle;
    private RecyclerView productRecycler;

    private double latitude = 0.0;
    private double longitude = 0.0;

    private int currentPage = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int TOTAL_PAGES = 100;
    private String searchKey = "";
    private boolean isNearest = false;

    private List<Product> productList;
    private ProductsAdapter productsAdapter;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_search);

        searchView = findViewById(R.id.searchView);
        nearest = findViewById(R.id.nearest);
        nearest.setOnClickListener(this);
        lowestPrice = findViewById(R.id.lowestPrice);
        lowestPrice.setOnClickListener(this);

        productRecycler = findViewById(R.id.recyclerView);
        toolbarTitle = findViewById(R.id.toolbarTitle);
        searchView.setImeActionLabel(getString(R.string.search), EditorInfo.IME_ACTION_DONE);
        searchView.setOnEditorActionListener(this);
        toolbarTitle.setText(getString(R.string.search));

        productList = new ArrayList<>();
        lowestPriceClick();
    }

    private void getNearestSearch() {

        checkLocationPermission();
        mFusedLocationProvider.getLastLocation().addOnSuccessListener(location -> {

            if (location != null) {

                latitude = location.getLatitude();
                longitude = location.getLongitude();
                productSearch(searchKey, latitude, longitude, String.valueOf(currentPage));
            } else {
                checkLocationPermission();
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        if (actionId == EditorInfo.IME_ACTION_DONE) {

            searchKey = Utils.getText(searchView);
            currentPage = 1;
            productList = new ArrayList<>();
            setProductsList();

            if (isNearest) {
                getNearestSearch();
            } else {
                // Handle pressing "Enter" key here
                productSearch(searchKey, latitude, longitude, String.valueOf(currentPage));
            }
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.nearest:

                nearestClick();
                break;
            case R.id.lowestPrice:

                lowestPriceClick();

                break;
        }
    }

    private void productSearch(String searchKey, double latitude, double longitude, String pageNo) {

        Call<List<Product>> call = serviceAPI.productSearch(searchKey, Utils.getCachedString(activity,
                Const.USER_ID, ""), latitude, longitude, pageNo, "10");
        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {

                    if (response.isSuccessful() && response.body().size() > 0) {

                        productsAdapter.removeLoadingFooter();
                        isLoading = false;
                        productsAdapter.addAll(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {

                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void lowestPriceClick() {
        nearest.setBackground(ContextCompat.getDrawable(activity,
                R.drawable.square_corners_gray_strokked));
        nearest.setTextColor(ContextCompat.getColor(activity, R.color.gray));

        lowestPrice.setBackground(ContextCompat.getDrawable(activity,
                R.drawable.rounded_corners_primary_button_bg));
        lowestPrice.setTextColor(ContextCompat.getColor(activity, R.color.white));

        latitude = 0.0;
        longitude = 0.0;
        isNearest = false;
    }

    private void nearestClick() {
        checkLocationPermission();
        nearest.setBackground(ContextCompat.getDrawable(activity,
                R.drawable.rounded_corners_primary_button_bg));
        nearest.setTextColor(ContextCompat.getColor(activity, R.color.white));

        lowestPrice.setBackground(ContextCompat.getDrawable(activity,
                R.drawable.square_corners_gray_strokked));
        lowestPrice.setTextColor(ContextCompat.getColor(activity, R.color.gray));

        isNearest = true;
    }

    private void setProductsList() {

        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 2);
        productRecycler.setLayoutManager(gridLayoutManager);
        productsAdapter = new ProductsAdapter(activity, productList, R.layout.item_home_product);
        productsAdapter.addAll(productList);
        productRecycler.setAdapter(productsAdapter);
        if (currentPage <= TOTAL_PAGES) productsAdapter.addLoadingFooter();
        else isLastPage = true;


        productRecycler.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNewPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    private void loadNewPage() {
        productSearch(searchKey, latitude, longitude, String.valueOf(currentPage));
    }

    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                logE("Location permissions granted, starting location");
            }

        }

    }
}
