package com.alqastas.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.CategoriesAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.CategoriesResponse;
import com.alqastas.models.responses.Category;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryActivity extends ParentActivity {

    private RecyclerView recyclerView;
    private CategoriesAdapter adapter;

    protected TextView toolbarTitle;
    protected ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_with_title);
        recyclerView = findViewById(R.id.recyclerView);
        toolbarTitle = findViewById(R.id.toolbarTitle);
        back = findViewById(R.id.back);

        toolbarTitle.setText(getString(R.string.categories));
        getCategories();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.back) {
            onBackPressed();
        }
    }

    private void setRecyclerView(List<Category> categoryList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        adapter = new CategoriesAdapter(activity, categoryList, R.layout.item_category_name);
        recyclerView.setAdapter(adapter);
    }

    private void getCategories() {

        Call<CategoriesResponse> categoriesCall = serviceAPI.getCategories("1" , "10");
        categoriesCall.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(@NonNull Call<CategoriesResponse> call, @NonNull Response<CategoriesResponse> response) {

                if (response.isSuccessful() && response.body().getCategoryList() != null) {
                    setRecyclerView(response.body().getCategoryList());
                }else {

                    logE("The response is " + new Gson().toJson(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CategoriesResponse> call, @NonNull Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }
}
