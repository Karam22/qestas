package com.alqastas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.alqastas.R;
import com.alqastas.language.LanguageHelper;
import com.alqastas.language.Languages;

public class SplashActivity extends AppCompatActivity {

    private int second = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashPostDelay();
    }


    private void splashPostDelay() {

        new Handler().postDelayed(() -> {

            LanguageHelper.changeLanguage(this, Languages.ARABIC);
            LanguageHelper.applyLanguage(this);

            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.fadein, R.anim.fadeout);
            finish();
        }, second);
    }
}