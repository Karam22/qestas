package com.alqastas.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.FavouriteProductsAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.HomeCategories;
import com.alqastas.models.responses.Product;
import com.alqastas.utils.Const;
import com.alqastas.utils.PaginationScrollListener;
import com.alqastas.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteProductsActivity extends ParentActivity {

    private TextView toolbarTitle;
    private RecyclerView recyclerView;

    private List<Product> productList;
    private FavouriteProductsAdapter productsAdapter;

    private int currentPage = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int TOTAL_PAGES = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_with_title);

        toolbarTitle = findViewById(R.id.toolbarTitle);
        recyclerView = findViewById(R.id.recyclerView);
        toolbarTitle.setText(R.string.favourite);

        productList = new ArrayList<>();
        setProductsList();
    }

    private void setProductsList() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(linearLayoutManager);
        productsAdapter = new FavouriteProductsAdapter(activity, productList, R.layout.item_product);
        productsAdapter.addAll(productList);
        recyclerView.setAdapter(productsAdapter);
        if (currentPage <= TOTAL_PAGES) productsAdapter.addLoadingFooter();
        else isLastPage = true;


        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNewPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void loadNewPage() {
        favouriteProducts(currentPage);
    }

    private void favouriteProducts(int pageNo) {

        Call<HomeCategories> call = serviceAPI.userFavouriteProducts(Long.parseLong(Utils.getCachedString(activity,
                Const.USER_ID, "0")), pageNo, 20);
        call.enqueue(new Callback<HomeCategories>() {
            @Override
            public void onResponse(Call<HomeCategories> call, Response<HomeCategories> response) {

                if (response.isSuccessful() && response.body().getProducts().size() > 0) {

                    productsAdapter.removeLoadingFooter();
                    isLoading = false;
                    productsAdapter.addAll(response.body().getProducts());

                    if (currentPage != TOTAL_PAGES) productsAdapter.addLoadingFooter();
                    else isLastPage = true;

                } else
                    productsAdapter.removeLoadingFooter();
            }

            @Override
            public void onFailure(Call<HomeCategories> call, Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }
}
