package com.alqastas.activities;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.HarajListAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.Product;
import com.alqastas.models.responses.ProductsResponse;
import com.alqastas.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HarajProductsActivity extends ParentActivity {

    private TextView toolbarTitle;
    private RecyclerView recyclerView;

    private int currentPage = 0;
    private int TOTAL_PAGES = 100;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private HarajListAdapter adapter;
    private List<Product> productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_with_title);

        toolbarTitle = findViewById(R.id.toolbarTitle);
        recyclerView = findViewById(R.id.recyclerView);

        toolbarTitle.setText(getString(R.string.haraj));

        productList = new ArrayList<>();
        setProductsList(productList);
    }

    private void setProductsList(List<Product> listProducts) {

        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new HarajListAdapter(activity, listProducts, R.layout.item_haraj_product);
        adapter.addAll(listProducts);
        recyclerView.setAdapter(adapter);
        if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
        else isLastPage = true;

        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNewPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    private void loadNewPage() {
        getHarajProducts(String.valueOf(currentPage));
    }

    private void getHarajProducts(String pageNo) {

//        if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
//        else isLastPage = true;

        Call<ProductsResponse> productsCall = serviceAPI.getHarajProducts(pageNo, "10");
        productsCall.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getProductList().size() > 0) {

                        adapter.removeLoadingFooter();
                        isLoading = false;
                        adapter.addAll(response.body().getProductList());

                    } else {
                        adapter.removeLoadingFooter();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

}
