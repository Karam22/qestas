package com.alqastas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.ChoosesAddressAdapter;
import com.alqastas.adapters.MyAddressesAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.Address;
import com.alqastas.utils.Const;
import com.alqastas.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAddressActivity extends ParentActivity {

    private TextView toolbarTitle;
    private TextView error;
    private Button addAddress;
    private RecyclerView recyclerView;

    private MyAddressesAdapter addressAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_with_button);

        toolbarTitle = findViewById(R.id.toolbarTitle);
        error = findViewById(R.id.error);
        addAddress = findViewById(R.id.save);
        addAddress.setOnClickListener(this);
        recyclerView = findViewById(R.id.recyclerView);

        addAddress.setText(getString(R.string.add_new_address));
        toolbarTitle.setText(getString(R.string.my_address));
        getUserAddress();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.save) {

            startActivity(new Intent(activity, AddAddressActivity.class)
                    .putExtra(Const.DESTINATION, Const.MY_ADDRESSES));
            finish();
        }
    }

    private void setAddressesRecycler(List<Address> addressList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        addressAdapter = new MyAddressesAdapter(activity, addressList, R.layout.item_my_addresses);
        recyclerView.setAdapter(addressAdapter);
    }

    private void handleEmptyError() {

        error.setVisibility(View.VISIBLE);
        error.setText(getString(R.string.no_addresses_add_one));
        recyclerView.setVisibility(View.GONE);
    }

    private void getUserAddress() {

        Call<List<Address>> call = serviceAPI.getUserAddresses(Utils.getCachedString(activity,
                Const.USER_ID, ""));
        call.enqueue(new Callback<List<Address>>() {
            @Override
            public void onResponse(@NonNull Call<List<Address>> call, @NonNull Response<List<Address>> response) {

                if (response.isSuccessful() && response.body() != null && response.body().size() > 0) {

                    setAddressesRecycler(response.body());

                } else {

                    handleEmptyError();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Address>> call, @NonNull Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

}
