package com.alqastas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.requests.RegisterRequest;
import com.alqastas.models.responses.DefaultResponse;
import com.alqastas.models.responses.UserResponse;
import com.alqastas.utils.Const;
import com.alqastas.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends ParentActivity implements View.OnClickListener {

    private TextInputEditText firstName;
    private TextInputEditText lastName;
    private TextInputEditText mobileNumber;
    private TextInputEditText email;
    private TextInputEditText password;
    private TextInputEditText confirmPassword;

    private TextView toolbarTitle;
    private ImageButton back;

    private Button btnSignUp;
    private boolean dataValid;

    private long TYPE_USER = 1;
    private long TYPE_STORE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        mobileNumber = findViewById(R.id.mobileNumber);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);

        btnSignUp = findViewById(R.id.signUp);
        btnSignUp.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.signUp:

                if (signUpValidate()) {

                    RegisterRequest registerRequest = new RegisterRequest();
                    registerRequest.setEmail(Utils.getText(email));
                    registerRequest.setFirstName(Utils.getText(firstName));
                    registerRequest.setLastname(Utils.getText(lastName));
                    registerRequest.setTelephone(Utils.getText(mobileNumber));
                    registerRequest.setLoginName(Utils.getText(mobileNumber));
                    registerRequest.setPWD(Utils.getText(password));
                    registerRequest.setUserTypeId(TYPE_USER);

                    signUpCall(registerRequest);
                }
                break;
            case R.id.back:
                onBackPressed();
                break;
        }
    }

    private boolean signUpValidate() {

        if (Utils.isEmpty(firstName)) {
            setEditError(firstName, R.string.filed_required);
        } else if (Utils.isEmpty(lastName)) {
            setEditError(lastName, R.string.filed_required);
        } else if (Utils.isEmpty(mobileNumber)) {
            setEditError(mobileNumber, R.string.filed_required);
        } else if (Utils.isEmpty(email)) {
            setEditError(email, R.string.filed_required);
        } else if (Utils.isEmpty(mobileNumber)) {
            setEditError(mobileNumber, R.string.filed_required);
        } else if (Utils.isEmpty(password)) {
            setEditError(password, R.string.filed_required);
        } else if (Utils.isEmpty(confirmPassword)) {
            setEditError(confirmPassword, R.string.filed_required);
        } else {
            dataValid = true;
        }
        return dataValid;
    }

    private void setEditError(EditText view, int error) {

        view.setError(getString(error));
        view.requestFocus();
        dataValid = false;
    }

    private void displayOTPDialog() {


    }

    private void signUpCall(RegisterRequest registerRequest) {

        showProgressDialog();
        Call<DefaultResponse> registerCall = serviceAPI.registerUser(registerRequest);
        registerCall.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {

                    loginCall(registerRequest.getTelephone(), registerRequest.getPWD());
                } else {

                    Utils.showShortToast(activity, R.string.phone_email_used_before);
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                hideProgressDialog();
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void loginCall(String mobileNumber, String password) {

        showProgressDialog();

        Call<UserResponse> userCall = serviceAPI.loginUser(mobileNumber, password, "password");
        userCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                hideProgressDialog();
                if (response.isSuccessful()) {

                    logE("The token : " + response.body().getToken());
                    Utils.cacheString(activity, Const.USER_TOKEN, response.body().getToken());
                    Utils.cacheString(activity, Const.USER_ID, response.body().getUserId());
                    Utils.cacheString(activity, Const.USER_MOBILE, mobileNumber);
                    Utils.showShortToast(activity, getString(R.string.welcome) + " "
                            + response.body().getDisplayName());
                    startActivity(new Intent(activity, HomeActivity.class));
                    finish();
                } else {
                    Utils.showShortToast(activity, R.string.mobile_passowrd_not_correct);
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                hideProgressDialog();
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(activity, LoginActivity.class));
        finish();
    }
}
