package com.alqastas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.adapters.OrderCartAdapter;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.dialogs.ConfirmationDialog;
import com.alqastas.models.requests.OrderDetail;
import com.alqastas.models.requests.OrderRequest;
import com.alqastas.models.responses.Address;
import com.alqastas.models.responses.Cart;
import com.alqastas.models.responses.PromoCodeResponse;
import com.alqastas.utils.Const;
import com.alqastas.utils.DialogUtils;
import com.alqastas.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends ParentActivity {

    private TextView userName;
    private TextView userPhone;
    private TextView userAddress;
    private TextView productsPrice;
    private TextView shippingExpenses;
    private TextView totalPrice;
    private TextView toolbarTitle;

    private TextInputEditText promoCode;
    private RecyclerView recyclerView;
    private Button confirm;
    private Address address;
    private OrderCartAdapter cartAdapter;
    private List<Cart> cartList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        initViews();
        getUserCart();
        setAddressData();
    }

    private void initViews() {
        userName = findViewById(R.id.userName);
        userPhone = findViewById(R.id.userPhone);
        userAddress = findViewById(R.id.userAddress);
        productsPrice = findViewById(R.id.productsPrice);
        shippingExpenses = findViewById(R.id.shippingExpenses);
        totalPrice = findViewById(R.id.totalPrice);
        toolbarTitle = findViewById(R.id.toolbarTitle);
        promoCode = findViewById(R.id.promoCode);
        recyclerView = findViewById(R.id.recyclerView);
        confirm = findViewById(R.id.confirm);
        confirm.setOnClickListener(this);

        address = (Address) getIntent().getSerializableExtra(Const.ADDRESS);
        cartList = new ArrayList<>();
        toolbarTitle.setText(getString(R.string.order_details));

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v.getId() == R.id.confirm) {
            // post the order here
            confirmOrder();
        }
    }

    private void confirmOrder() {

        if (Utils.isEmpty(promoCode)) {

            displayDialogSendOrder(getString(R.string.sure_send_order));

        } else {
            validatePromoCode();
        }
    }

    private void sendOrder() {

        List<OrderDetail> orderDetailList = new ArrayList<>();

        OrderRequest orderRequest = new OrderRequest();
        if (!Utils.isEmpty(promoCode)) {
            orderRequest.setPromoCode(Utils.getText(promoCode));
        }
        orderRequest.setUserAddressId(address.getId());
        orderRequest.setUserId(Long.parseLong(Utils.getCachedString(activity, Const.USER_ID, "0")));

        for (int i = 0; i < cartList.size(); i++) {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setItemId(cartList.get(i).getId());
            orderDetail.setQuantity((long) cartList.get(i).getQty());

            orderDetailList.add(orderDetail);
        }
        orderRequest.setOrderDetails(orderDetailList);
        sendOrderCall(orderRequest);
    }

    private void setAddressData() {

        userName.setText(Utils.getCachedString(activity, Const.USER_NAME, "Karam"));
        userPhone.setText(Utils.getCachedString(activity, Const.USER_MOBILE, ""));
        userAddress.setText(address.getAddress());
    }

    private void setCartRecycler() {

        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        cartAdapter = new OrderCartAdapter(activity, cartList, R.layout.item_order_cart);
        recyclerView.setAdapter(cartAdapter);
    }

    private void calculateTotalPrice() {

        float price = 0;
        for (int i = 0; i < cartList.size(); i++) {
            price = price + (Float.parseFloat(cartList.get(i).getPrice()) * cartList.get(i).getQty());
        }
        productsPrice.setText(price + " " + getString(R.string.rs));
        try {
            shippingExpenses.setText(address.getShippingPrice() + " " + getString(R.string.rs));
            price = price + Float.parseFloat(address.getShippingPrice());
        } catch (NumberFormatException e) {
            shippingExpenses.setText("20");
            price = price + 20;
        }
        totalPrice.setText(String.format("%s %s", String.valueOf(price), getString(R.string.rs)));
    }

    private void getUserCart() {

        Call<List<Cart>> call = serviceAPI.getCartItems(Utils.getCachedString(activity,
                Const.USER_ID, ""));
        call.enqueue(new Callback<List<Cart>>() {
            @Override
            public void onResponse(@NonNull Call<List<Cart>> call, @NonNull Response<List<Cart>> response) {

                if (response.isSuccessful() && response.body() != null && response.body().size() > 0) {

                    cartList = response.body();
                    setCartRecycler();
                    calculateTotalPrice();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Cart>> call, @NonNull Throwable t) {

                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void validatePromoCode() {

        Call<List<PromoCodeResponse>> call = serviceAPI.validatePromoCode(Utils.getText(promoCode));
        call.enqueue(new Callback<List<PromoCodeResponse>>() {
            @Override
            public void onResponse(Call<List<PromoCodeResponse>> call, Response<List<PromoCodeResponse>> response) {

                if (response.isSuccessful() && response.body().size() > 0) {

                    float myPrice = Float.parseFloat(Utils.getText(totalPrice).replace(getString(R.string.rs), ""))
                            - response.body().get(0).getAmount();
                    if (myPrice > 0) {

                        displayDialogSendOrder(myPrice + " " + getString(R.string.rs) + " " +
                                getString(R.string.price_after_discount) + " .. " + getString(R.string.sure_send_order));
                    }
                } else {
                    promoCode.setError(getString(R.string.promoNotValid));
                }
            }

            @Override
            public void onFailure(Call<List<PromoCodeResponse>> call, Throwable t) {
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void sendOrderCall(OrderRequest orderRequest) {

        showProgressDialog();
        Call<String> orderCall = serviceAPI.postUserOrder(orderRequest);
        orderCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideProgressDialog();
                if (response.isSuccessful()) {

                    Utils.showLongToast(activity, getString(R.string.recieveSuccessfully));
                    startActivity(new Intent(activity, HomeActivity.class));
                    finishAffinity();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideProgressDialog();
                RetrofitClientInstance.checkFailureType(t, activity);
            }
        });
    }

    private void displayDialogSendOrder(String message) {

        ConfirmationDialog dialog = new ConfirmationDialog(activity, message);
        dialog.show();
        dialog.setDialogResult(this::sendOrder);
    }
}
