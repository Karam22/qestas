package com.alqastas.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.activities.ProductListActivity;
import com.alqastas.models.responses.Category;
import com.alqastas.utils.Const;

import java.util.List;

public class CategoriesAdapter extends ParentRecyclerAdapter<Category> {

    public CategoriesAdapter(Context context, List<Category> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new CategoriesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;

        final Category item = data.get(position);
        holder.title.setText(item.getTitleAr());
        holder.itemView.setOnClickListener(v -> {

            context.startActivity(new Intent(context, ProductListActivity.class)
                    .putExtra(Const.CATEGORY_ID, item.getId())
                    .putExtra(Const.CATEGORY_NAME, item.getTitleAr()));
        });
    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private TextView title;

        ViewHolder(final View itemView) {
            super(context, itemView);
            title = itemView.findViewById(R.id.title);
        }
    }
}
