package com.alqastas.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.activities.ProductListActivity;
import com.alqastas.models.responses.HomeCategories;
import com.alqastas.utils.Const;

import java.util.List;

public class HomeListAdapter extends ParentRecyclerAdapter<HomeCategories> {

    public HomeListAdapter(Context context, List<HomeCategories> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;

        final HomeCategories item = data.get(position);
        holder.categoryName.setText(item.getCategoryDescAr());

        holder.recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        adapter = new ProductsAdapter(context, item.getProducts(), R.layout.item_home_product);
        holder.recyclerView.setAdapter(adapter);

        holder.viewAll.setOnClickListener(v -> {

            context.startActivity(new Intent(context, ProductListActivity.class)
                    .putExtra(Const.CATEGORY_ID, item.getCategoryId())
                    .putExtra(Const.CATEGORY_NAME, item.getCategoryDescAr()));
        });
    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private TextView categoryName;
        private TextView viewAll;
        private RecyclerView recyclerView;

        ViewHolder(final View itemView) {
            super(context, itemView);
            categoryName = itemView.findViewById(R.id.categoryName);
            viewAll = itemView.findViewById(R.id.viewAll);
            recyclerView = itemView.findViewById(R.id.recyclerView);
        }
    }
}

