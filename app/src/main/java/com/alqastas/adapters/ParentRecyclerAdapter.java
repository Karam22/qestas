package com.alqastas.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;


import com.alqastas.R;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.connections.ServiceAPI;
import com.alqastas.utils.DialogUtils;
import com.alqastas.utils.Utils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Karam on 5/11/16.
 */
public abstract class ParentRecyclerAdapter<Item> extends RecyclerView.Adapter<ParentRecyclerViewHolder> {
    protected ParentRecyclerAdapter adapter;
    protected Context context;
    protected List<Item> data;
    protected int layoutId;
    protected ProgressDialog progressDialog;
    protected ServiceAPI serviceAPI;

    // used to hold connection handlers that should be cancelled when destroyed

    public ParentRecyclerAdapter(Context context, List<Item> data) {
        this(context, data, -1);
    }

    public ParentRecyclerAdapter(Context context, List<Item> data, int layoutId) {
        inti(context, data, layoutId);
    }

    public ParentRecyclerAdapter(Context context, Item[] data) {
        this(context, data, -1);
    }

    public ParentRecyclerAdapter(Context context, Item[] data, int layoutId) {
        inti(context, Arrays.asList(data), layoutId);
    }

    private void inti(Context context, List<Item> data, int layoutId) {
        this.context = context;
        this.data = data;
        this.layoutId = layoutId;

        adapter = this;
        serviceAPI = RetrofitClientInstance.getRetrofitInstance().create(ServiceAPI.class);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public int getResColor(int id) {
        return context.getResources().getColor(id);
    }

    protected void logE(String msg) {
        Utils.logE(msg);
    }

    protected String getString(int resId) {
        return context.getString(resId);
    }

    public void showProgressDialog() {
        if (progressDialog != null) {
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
        } else {
            progressDialog = DialogUtils.showProgressDialog(context, R.string.please_wait_dotted);
        }
    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        // cancel requests if found

        super.onDetachedFromRecyclerView(recyclerView);
    }

    public void removeItem(int position) {
        try {

            data.remove(position);
            notifyDataSetChanged();

        }catch (IndexOutOfBoundsException e){

            notifyDataSetChanged();
        }
    }

    public Context getContext() {
        return context;
    }
}
