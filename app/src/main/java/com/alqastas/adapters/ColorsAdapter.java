package com.alqastas.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alqastas.R;
import com.alqastas.activities.ProductListActivity;
import com.alqastas.models.responses.ItemColor;
import com.alqastas.utils.Const;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

public class ColorsAdapter extends ParentRecyclerAdapter<ItemColor> {

    private int lastCheckedPosition = 0;

    public ColorsAdapter(Context context, List<ItemColor> data, int layoutId) {
        super(context, data, layoutId);
    }

    public long getCheckedItem() {

        return data.get(lastCheckedPosition).getId();
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new ColorsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;

        final ItemColor item = data.get(position);
        holder.color.setBackgroundColor((int) Color.alpha(item.getColorId()));

        if (position == lastCheckedPosition) {
            holder.layColor.setBackground(ContextCompat.getDrawable(context, R.drawable.round_selected));
        } else {
            holder.layColor.setBackground(ContextCompat.getDrawable(context, R.drawable.round_not_selected));
        }
        holder.itemView.setOnClickListener(v -> {

            context.startActivity(new Intent(context, ProductListActivity.class)
                    .putExtra(Const.CATEGORY_ID, item.getId()));
        });
    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private RoundedImageView color;
        private LinearLayout layColor;

        ViewHolder(final View itemView) {
            super(context, itemView);
            color = itemView.findViewById(R.id.color);
            layColor = itemView.findViewById(R.id.layColor);

            color.setOnClickListener(v -> {
                lastCheckedPosition = getAdapterPosition();
                // TO Do The Action When Choose The color on the adapter
            });
        }
    }
}
