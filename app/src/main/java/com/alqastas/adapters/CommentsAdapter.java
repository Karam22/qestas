package com.alqastas.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.models.responses.ItemReview;

import java.util.List;

public class CommentsAdapter extends ParentRecyclerAdapter<ItemReview> {

    public CommentsAdapter(Context context, List<ItemReview> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;

        final ItemReview item = data.get(position);
        holder.userName.setText(item.getReviewerName());
        holder.comment.setText(item.getComments());
        holder.comment.setText(item.getComments());
        holder.ratingBar.setRating(item.getRate());
    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private TextView userName;
        private TextView comment;
        private AppCompatRatingBar ratingBar;

        ViewHolder(final View itemView) {
            super(context, itemView);
            userName = itemView.findViewById(R.id.userName);
            comment = itemView.findViewById(R.id.comment);
            ratingBar = itemView.findViewById(R.id.rateBar);
        }
    }
}
