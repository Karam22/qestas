package com.alqastas.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.models.responses.Cart;

import java.util.List;

public class OrderCartAdapter extends ParentRecyclerAdapter<Cart> {

    public OrderCartAdapter(Context context, List<Cart> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;

        final Cart item = data.get(position);
        holder.itemTitle.setText(item.getTitleAR());
        holder.itemPrice.setText(item.getPrice() + " " + getString(R.string.rs));
        holder.itemQty.setText(getString(R.string.count) + " " + item.getQty());
        if (item.getDescription() == null || item.getDescription().equals("")) {
            holder.itemDescription.setVisibility(View.GONE);
        } else {
            holder.itemDescription.setText(item.getDescription());
        }
    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private TextView itemTitle;
        private TextView itemPrice;
        private TextView itemQty;
        private TextView itemDescription;

        ViewHolder(final View itemView) {
            super(context, itemView);

            itemTitle = itemView.findViewById(R.id.itemTitle);
            itemQty = itemView.findViewById(R.id.itemQty);
            itemDescription = itemView.findViewById(R.id.itemDescription);
            itemPrice = itemView.findViewById(R.id.itemPrice);

        }
    }
}