package com.alqastas.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.models.DeleteCartListener;
import com.alqastas.models.UpdateCartListener;
import com.alqastas.models.responses.Cart;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class UserCartAdapter extends ParentRecyclerAdapter<Cart> {

    public UserCartAdapter(Context context, List<Cart> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;

        final Cart item = data.get(position);
        holder.itemTitle.setText(item.getTitleAR());
        holder.itemPrice.setText(item.getPrice() + " " + getString(R.string.rs));

        holder.itemCount.setText(String.valueOf(item.getQty()));

        if (item.getImage() != null) {
            Picasso.get().load(item.getImage()).centerCrop().into(holder.itemImage);
        }

        holder.deleteItem.setOnClickListener(v -> {
            EventBus.getDefault().post(new DeleteCartListener(item.getId(), holder.getAdapterPosition()));
        });
        holder.plus.setOnClickListener(v -> {

            int newQty = item.getQty() + 1;
            item.setQty(newQty);
            holder.itemCount.setText(String.valueOf(newQty));
            EventBus.getDefault().post(new UpdateCartListener(item.getId(), item.getQty(), holder.getAdapterPosition()));

        });
        holder.minus.setOnClickListener(v -> {

            int newQty = item.getQty() - 1;
            if (newQty > 0) {
                item.setQty(newQty);
                holder.itemCount.setText(String.valueOf(newQty));
                EventBus.getDefault().post(new UpdateCartListener(item.getId(), item.getQty(), position));
            } else {

                //Delete The item from the cart
                EventBus.getDefault().post(new DeleteCartListener(item.getId(), position));
            }
        });

    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private TextView itemTitle;
        private TextView itemSize;
        private TextView itemColor;
        private TextView itemPrice;
        private TextView itemCount;

        private ImageView itemImage;
        private ImageButton deleteItem;
        private ImageButton plus;
        private ImageButton minus;

        ViewHolder(final View itemView) {
            super(context, itemView);
            itemTitle = itemView.findViewById(R.id.itemTitle);
            itemSize = itemView.findViewById(R.id.itemSize);
            itemColor = itemView.findViewById(R.id.itemColor);
            itemPrice = itemView.findViewById(R.id.itemPrice);
            itemCount = itemView.findViewById(R.id.itemCount);

            itemImage = itemView.findViewById(R.id.itemImage);
            deleteItem = itemView.findViewById(R.id.deleteItem);
            plus = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);

        }
    }
}
