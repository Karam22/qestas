package com.alqastas.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.connections.RetrofitClientInstance;
import com.alqastas.models.responses.Address;
import com.alqastas.models.responses.DefaultResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAddressesAdapter extends ParentRecyclerAdapter<Address> {

    public MyAddressesAdapter(Context context, List<Address> data, int layoutId) {
        super(context, data, layoutId);
    }


    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;

        final Address item = data.get(position);
        holder.address.setText(item.getAddress());

        holder.remove.setOnClickListener(v -> {

            removeUserAddress(item.getId(),position);
        });
    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private TextView address;
        private ImageButton remove;

        ViewHolder(final View itemView) {
            super(context, itemView);
            address = itemView.findViewById(R.id.address);
            remove = itemView.findViewById(R.id.remove);
        }
    }

    private void removeUserAddress(long userAddressId , int position) {

        Call<DefaultResponse> call = serviceAPI.removeUserAddress(userAddressId);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {

                if (response.isSuccessful()){

                    removeItem(position);
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                RetrofitClientInstance.checkFailureType(t,context);
            }
        });
    }
}
