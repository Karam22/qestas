package com.alqastas.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.models.LoadingProductsListener;
import com.alqastas.models.responses.Category;
import com.alqastas.models.responses.SubCategory;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class SubCategoryAdapter extends ParentRecyclerAdapter<SubCategory> {

    public SubCategoryAdapter(Context context, List<SubCategory> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new SubCategoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {
        final SubCategoryAdapter.ViewHolder holder = (SubCategoryAdapter.ViewHolder) viewHolder;

        final SubCategory item = data.get(position);
        holder.title.setText(item.getTitle());
        if (item.getImage() != null) {
            Picasso.get().load(item.getImage()).centerCrop().into(holder.image);
        }
        holder.itemView.setOnClickListener(v -> {

            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getId() == item.getId()) {
                    item.setChecked(true);
                } else {
                    item.setChecked(false);
                }
            }
        });

        if (item.isChecked()) {
            EventBus.getDefault().post(new LoadingProductsListener(item.getId()));
            holder.title.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else {
            holder.title.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private TextView title;
        private ImageView image;

        ViewHolder(final View itemView) {
            super(context, itemView);
            title = itemView.findViewById(R.id.title);
            image = itemView.findViewById(R.id.imageView);
        }
    }
}
