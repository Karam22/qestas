package com.alqastas.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.activities.UserProductDesActivity;
import com.alqastas.models.responses.Product;
import com.alqastas.utils.Const;
import com.alqastas.utils.PaginationAdapterCallback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HarajListAdapter extends ParentRecyclerAdapter<Product> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private String errorMsg;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private PaginationAdapterCallback mCallback;

    public HarajListAdapter(Context context, List<Product> data, int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View viewItem;
        ParentRecyclerViewHolder viewHolder = null;
        if (viewType == ITEM) {
            viewItem = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
            viewHolder = new ViewHolder(viewItem);

        } else if (viewType == LOADING) {
            viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
            viewHolder = new LoadingVH(viewItem);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {

        switch (getItemViewType(position)) {

            case ITEM:
                final ViewHolder holder = (ViewHolder) viewHolder;
                final Product item = data.get(position);
                holder.title.setText(item.getItemNameAR());
                holder.price.setText(item.getItemPrice() + " " + getString(R.string.rs));


                if (item.getImagesPath() != null) {
                    Picasso.get().load(Const.IMAGE_URL + item.getImagesPath()).into(holder.image);
                } else {
                    holder.image.setImageResource(R.drawable.logo);
                }

                if (item.getItemSellerName() != null) {
                    holder.sellerName.setText(getString(R.string.from) + " " + item.getItemSellerName());
                } else {
                    holder.sellerName.setText(getString(R.string.from) + " " + getString(R.string.app_name));
                }

                holder.itemView.setOnClickListener(v -> {

                    context.startActivity(new Intent(context, UserProductDesActivity.class)
                            .putExtra(Const.PRODUCT_ID, item.getID()));
                });


                break;
            case LOADING:
                final LoadingVH loadingHolder = (LoadingVH) viewHolder;

                if (retryPageLoad) {
                    loadingHolder.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingHolder.mProgressBar.setVisibility(View.GONE);

                    loadingHolder.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.something_went_wrong_try_again));

                } else {
                    loadingHolder.mErrorLayout.setVisibility(View.GONE);
                    loadingHolder.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;

        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public int getItemViewType(int position) {

        return (position == data.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private TextView title;
        private TextView sellerName;
        private ImageView image;
        private TextView price;

        ViewHolder(final View itemView) {
            super(context, itemView);
            title = itemView.findViewById(R.id.title);
            sellerName = itemView.findViewById(R.id.sellerName);
            image = itemView.findViewById(R.id.image);
            price = itemView.findViewById(R.id.price);
        }
    }

    class LoadingVH extends ParentRecyclerViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        LoadingVH(View itemView) {
            super(context, itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadMore);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

     /*
        Helpers - Pagination
   _________________________________________________________________________________________________
    */

    public void add(Product r) {
        data.add(r);
        notifyItemInserted(data.size() - 1);
    }

    public void addAll(List<Product> products) {
        for (Product result : products) {
            add(result);
        }
    }

    public void remove(Product r) {
        int position = data.indexOf(r);
        if (position > -1) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Product());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = data.size() - 1;
        Product result = getItem(position);

        if (result != null) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Product getItem(int position) {
        return data.get(position);
    }

    private void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) this.errorMsg = errorMsg;
    }
}
