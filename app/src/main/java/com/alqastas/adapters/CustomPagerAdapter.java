package com.alqastas.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alqastas.R;
import com.alqastas.models.responses.ItemImage;
import com.alqastas.utils.Const;
import com.alqastas.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by karam on 3/1/17.
 */

public class CustomPagerAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<ItemImage> images;

    public CustomPagerAdapter(Context context, List<ItemImage> images) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = images;
    }


    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = itemView.findViewById(R.id.imageView);

        try {
            Picasso.get().load(Const.IMAGE_URL + images.get(position).getImagesPath())
                    .centerInside().resize(500,500).into(imageView);
        } catch (IllegalArgumentException e) {
            Utils.showShortToast(mContext, mContext.getString(R.string.something_went_wrong));
        }
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}