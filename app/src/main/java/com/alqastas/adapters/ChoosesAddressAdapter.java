package com.alqastas.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.models.responses.Address;

import java.util.List;

public class ChoosesAddressAdapter extends ParentRecyclerAdapter<Address> {

    private int lastCheckedPosition = 0;

    public ChoosesAddressAdapter(Context context, List<Address> data, int layoutId) {
        super(context, data, layoutId);
    }

    public Address getCheckedItem() {

        return data.get(lastCheckedPosition);
    }


    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder viewHolder, final int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;

        final Address item = data.get(position);
        holder.address .setText(item.getAddress());
        holder.radio.setChecked(position == lastCheckedPosition);
    }

    class ViewHolder extends ParentRecyclerViewHolder {

        private TextView address;
        private RadioButton radio;

        ViewHolder(final View itemView) {
            super(context, itemView);
            address = itemView.findViewById(R.id.address);
            radio = itemView.findViewById(R.id.radio);

            radio.setOnClickListener(v -> {
                lastCheckedPosition = getAdapterPosition();
                notifyDataSetChanged();
            });
        }
    }
}
