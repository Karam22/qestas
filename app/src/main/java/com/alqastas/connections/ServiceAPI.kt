package com.alqastas.connections

import com.alqastas.models.requests.OrderRequest
import com.alqastas.models.requests.RegisterRequest
import com.alqastas.models.responses.*
import retrofit2.Call
import retrofit2.http.*

interface ServiceAPI {

    @GET("/api/Category/GetCategories")
    fun getCategories(@Query("pageno") pageNo: String,
                      @Query("pagesize") pageSize: String): Call<CategoriesResponse>


    @GET("/api/Category/GetsubCategories")
    fun getSubCategories(@Query("MainCategoryId") categoryId: String,
                         @Query("pageno") pageNo: String,
                         @Query("pagesize") pageSize: String): Call<SubCategoriesResponse>

    @GET("/api/Cart/GetCartItems")
    fun getCartItems(@Query("UserId") userId: String): Call<List<Cart>>

    @GET("/api/Home/GetHome")
    fun getHomeResponse(@Query("userid") userId: String): Call<HomeResponse>

    @GET("/api/Orders/ValidatePromocode")
    fun validatePromoCode(@Query("promocode") promoCode: String): Call<List<PromoCodeResponse>>


    @GET("/api/Items/GetItemReviews")
    fun getProductReviews(@Query("productId") productId: Long,
                          @Query("PageNo") pageNo: String,
                          @Query("pagesize") pageSize: String): Call<ReviewsResponse>

    @GET("/api/Items/GetProductDetail")
    fun getProductDetails(@Query("productId") productId: Long,
                          @Query("userId") userId: String): Call<ProductDetails>

    @GET("/api/Home/GetAnnoncments")
    fun getAnnouncement(): Call<List<AnnoncmentResponse>>

    @GET("/api/User/GetuserAddress")
    fun getUserAddresses(@Query("UserId") userId: String): Call<List<Address>>


    @GET("/api/Category/Getproducts")
    fun getProducts(@Query("MainCategoryId") categoryId: String,
                    @Query("SubCategoryId") SubCategoryId: String,
                    @Query("userid") userId: String,
                    @Query("pageno") pageNo: String,
                    @Query("pagesize") pageSize: String): Call<ProductsResponse>

    @GET("/api/Haraj/Getproducts")
    fun getHarajProducts(@Query("pageno") pageNo: String,
                         @Query("pagesize") pageSize: String): Call<ProductsResponse>

    @GET("/api/Product/ProductSearch")
    fun productSearch(@Query("Searchkeywoard") keyword: String,
                      @Query("userId") userId: String,
                      @Query("Latitude") latitude: Double,
                      @Query("Longitude") longitude: Double,
                      @Query("Page") page: String,
                      @Query("Length") length: String): Call<List<Product>>

    @GET("/api/Product/GetFavoriteProducts")
    fun userFavouriteProducts(
            @Query("userid") userId: Long,
            @Query("pageno") page: Int,
            @Query("pagesize") length: Int): Call<HomeCategories>

    @GET("/api/User/RemoveuserAddress")
    fun removeUserAddress(
            @Query("UserAddressId") userAddressId: Long): Call<DefaultResponse>

    @FormUrlEncoded
    @POST("/token")
    fun loginUser(@Field("username") userName: String, @Field("password") password: String,
                  @Field("grant_type") grantType: String): Call<UserResponse>

    // Register New User

    @POST("/api/User/RegisterNewUser")
    fun registerUser(@Body registerRequest: RegisterRequest): Call<DefaultResponse>

    @FormUrlEncoded
    @POST("/api/Items/PostFavouriteProduct")
    fun postUserFavourite(@Field("ProductId") productId: Long,
                          @Field("AddReomve") status: Boolean,
                          @Field("UserId") uerId: String): Call<DefaultResponse>

    @FormUrlEncoded
    @POST("/api/Home/PostAnnoncmentClicked")
    fun postAnnouncementClick(@Field("Annoncmentid") announcementId: Long,
                              @Field("UserId") uerId: String): Call<DefaultResponse>


    @POST("/api/Orders/PostOrder")
    fun postUserOrder(@Body orderRequest: OrderRequest): Call<String>

    //Adding address
    @FormUrlEncoded
    @POST("/api/User/RegisterUserAddress")
    fun AddUserAddress(@Field("UserId") userId: String,
                       @Field("Address") address: String): Call<DefaultResponse>

    //Update user Cart
    @FormUrlEncoded
    @POST("/api/Cart/UpdateCartItem")
    fun updateUserCart(@Field("CartItemId") cartId: Long,
                       @Field("Quantity") quantity: Int): Call<List<DefaultResponse>>

    //Add user Cart
    @FormUrlEncoded
    @POST("/api/Cart/PostCartItem")
    fun postUserCart(@Field("ItemId") itemId: Long,
                     @Field("Quantity") quantity: Int,
                     @Field("userid") userId: String): Call<List<DefaultResponse>>

    @POST("/api/Cart/RemoveCartItem")
    fun deleteUserCart(@Query("cartItem") cartId: Long): Call<List<DefaultResponse>>

}
