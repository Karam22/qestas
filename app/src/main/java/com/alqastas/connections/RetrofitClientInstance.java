package com.alqastas.connections;

import android.content.Context;

import com.alqastas.R;
import com.alqastas.utils.Utils;

import java.io.IOException;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL = "http://alqstasapi.almunawirco.com";

    public static Retrofit getRetrofitInstance() {

        if (retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static void checkFailureType(Throwable t, Context context) {

        if (t instanceof IOException) {

            Utils.showShortToast(context, R.string.connectionProblem);
        } else {
            Utils.showShortToast(context,
                    context.getString(R.string.Conversion_issue));
        }
    }
}
