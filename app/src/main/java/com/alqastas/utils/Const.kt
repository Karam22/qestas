package com.alqastas.utils

object Const {
    // app constants
    const val END_POINT = "http://142.93.46.191/api/"
    const val PREFS_NAME = "alqastas"
    const val LOG_TAG = "AlQastas Parents"
    const val APP_FILES_DIR = "/.alqastas"

    // keys
    const val KEY_ACTIVE_USER = "active_user"
    const val KEY_FCM_TOKEN_UPDATED = "fcm_token_updated"
    //User
    const val USER_NAME = "userName"
    const val USER_TOKEN = "userToken"
    const val USER_ID = "userID"
    const val USER_MOBILE = "userMobile"
    const val USER_EMAIL= "userEmail"
    const val DESTINATION= "destination"
    const val CHOOSE_ADDRESS= "chooseAddress"
    const val MY_ADDRESSES= "myAddress"

    //App Extras
    const val CATEGORY_ID= "category_ID"
    const val CATEGORY_NAME= "category_NAME"
    const val PRODUCT_ID= "product_ID"
    const val ADDRESS= "address"
    const val IMAGE_URL= "http://www.almunawirco.com/images/products/"

}