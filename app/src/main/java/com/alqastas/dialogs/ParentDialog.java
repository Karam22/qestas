package com.alqastas.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alqastas.R;
import com.alqastas.utils.Utils;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Karam on 2/17/2016.
 */
public class ParentDialog extends Dialog implements View.OnClickListener {
    // used to hold connection handlers that should be cancelled when destroyed

    protected Context context;
    protected FrameLayout rootView;
    private TextView tvDialogTitle;
    private ImageButton ibClose;
    private View progressView;

    public ParentDialog(Context context) {
        super(context);
        this.context = context;
        setCancelable(false);

        // set no title and transparent bg
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        // init and customize the dialog toolbar
        rootView = findViewById(android.R.id.content);
        View toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            tvDialogTitle = findViewById(R.id.tvDialogTitle);
            ibClose = findViewById(R.id.ibClose);
            ibClose.setOnClickListener(this);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (tvDialogTitle != null) {
            tvDialogTitle.setText(title);
        }
    }

    @Override
    public void setTitle(int titleId) {
        setTitle(context.getString(titleId));
    }

    @Override
    public void setCancelable(boolean flag) {
        super.setCancelable(flag);
        if (ibClose != null) {
            ibClose.setVisibility(flag ? View.VISIBLE : View.GONE);
        }
    }

    public void logE(String msg) {
        Utils.logE(msg);
    }

    public String getString(int strId) {
        return context.getString(strId);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ibClose) {
            dismiss();
        } else {
            logE("onClick has been invoked from ParentDialog");
        }
    }

    public void showProgressView() {
        hideProgressView();

        if (rootView != null && progressView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            progressView = inflater.inflate(R.layout.view_dialog_progress, null);
            rootView.addView(progressView);
        }
        progressView.setVisibility(View.VISIBLE);
        super.setCancelable(false);
    }

    public void hideProgressView() {
        if (progressView != null) {
            progressView.setVisibility(View.GONE);
        }
        super.setCancelable(true);
    }


    @Override
    public void dismiss() {
        // cancel requests if found


        super.dismiss();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        dismiss();

    }

    @Override
    public void show() {
        super.show();

        // customize the dialog width
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(layoutParams);
    }
}
