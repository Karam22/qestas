package com.alqastas.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alqastas.R;

public class ConfirmationDialog extends ParentDialog {

    private TextView tvMessage;
    private Button yes;
    private Button no;
    OnMyDialogResult mDialogResult; // the callback

    public ConfirmationDialog(Context context, String message) {
        super(context);
        setContentView(R.layout.dialog_confirmation);

        tvMessage = findViewById(R.id.message);
        yes = findViewById(R.id.yes);
        no = findViewById(R.id.no);

        tvMessage.setText(message);

        no.setOnClickListener(v -> {
            dismiss();
        });

        yes.setOnClickListener(v -> {
            mDialogResult.yes();
            dismiss();
        });
    }

    public void setDialogResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    public interface OnMyDialogResult {
        void yes();
    }
}
