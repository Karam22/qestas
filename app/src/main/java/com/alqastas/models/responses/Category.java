package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Category implements Serializable {

    @SerializedName("CategoryId")
    private long id;
    @SerializedName("CategoryDesc")
    private String title;
    @SerializedName("CategoryDescAr")
    private String titleAr;


    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleAr() {
        return titleAr;
    }
}
