
package com.alqastas.models.responses;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemImage implements Serializable
{

    @SerializedName("ItemImagesId")
    @Expose
    private Long itemImagesId;
    @SerializedName("ImagesPath")
    @Expose
    private String imagesPath;
    @SerializedName("ItemId")
    @Expose
    private Long itemId;
    private final static long serialVersionUID = 2944601270453162466L;

    public Long getItemImagesId() {
        return itemImagesId;
    }

    public void setItemImagesId(Long itemImagesId) {
        this.itemImagesId = itemImagesId;
    }

    public String getImagesPath() {
        return imagesPath;
    }

    public void setImagesPath(String imagesPath) {
        this.imagesPath = imagesPath;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

}
