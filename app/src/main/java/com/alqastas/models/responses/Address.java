package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Address implements Serializable {

    @SerializedName("UserAddressID")
    private long id;

    @SerializedName("Address")
    private String address;

    @SerializedName("ShippingPrice")
    private String shippingPrice;

    public long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShippingPrice() {
        return shippingPrice;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setShippingPrice(String shippingPrice) {
        this.shippingPrice = shippingPrice;
    }
}
