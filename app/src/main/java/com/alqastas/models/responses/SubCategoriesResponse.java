package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SubCategoriesResponse extends DefaultResponse implements Serializable {

    @SerializedName("data")
    private List<SubCategory> subCategoryList;

    public List<SubCategory> getSubCategoryList() {
        return subCategoryList;
    }
}
