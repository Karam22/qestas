package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListProduct implements Serializable {

    @SerializedName("ID")
    private long id;
    @SerializedName("ItemName")
    private String title;
    @SerializedName("itemPrice")
    private String price;
    @SerializedName("ImagesPath")
    private String image;
    @SerializedName("isFavourite")
    private int isFavourite;

    @SerializedName("ItemName_AR")
    private int itemNameAR;

    @SerializedName("itemSellerName")
    private int itemSellerName;

    public long getId() {
        return id;
    }

    public int getIsFavourite() {
        return isFavourite;
    }

    public String getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public int getItemNameAR() {
        return itemNameAR;
    }

    public int getItemSellerName() {
        return itemSellerName;
    }
}
