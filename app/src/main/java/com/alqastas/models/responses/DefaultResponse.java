package com.alqastas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ahmed Karam on 1/21/2018.
 */

public class DefaultResponse {


    @SerializedName("MessageTitle")
    @Expose
    private String message;
    @SerializedName("MessageTitle_AR")
    @Expose
    private String messageAr;
    @SerializedName("Message")
    @Expose
    private String error;

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }

    public String getMessageAr() {
        return messageAr;
    }
}
