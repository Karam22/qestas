package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Cart implements Serializable {

    @SerializedName("ShoppingCartID")
    private long id;
    @SerializedName("ItemId")
    private long itemId;
    @SerializedName("ItemName_AR")
    private String titleAR;
    @SerializedName("ItemName")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("image")
    private String image;
    @SerializedName("Price")
    private String price;
    @SerializedName("Quantity")
    private int qty;
    @SerializedName("PriceChange")
    private boolean PriceChange;

    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }

    public int getQty() {
        return qty;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTitleAR() {
        return titleAR;
    }
}
