
package com.alqastas.models.responses;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product implements Serializable
{

    @SerializedName("CategoryId")
    @Expose
    private Long categoryId;
    @SerializedName("CategoryDescAr")
    @Expose
    private String categoryDescAr;
    @SerializedName("CategoryDesc")
    @Expose
    private String categoryDesc;
    @SerializedName("SubCategoryId")
    @Expose
    private Long subCategoryId;
    @SerializedName("SubCategoryDescAr")
    @Expose
    private String subCategoryDescAr;
    @SerializedName("SubCategoryDesc")
    @Expose
    private String subCategoryDesc;
    @SerializedName("ID")
    @Expose
    private Long iD;
    @SerializedName("ItemName_AR")
    @Expose
    private String itemNameAR;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("ImagesPath")
    @Expose
    private String imagesPath;
    @SerializedName("itemSellerName")
    @Expose
    private String itemSellerName;
    @SerializedName("itemPrice")
    @Expose
    private Long itemPrice;
    @SerializedName("isFavourite")
    @Expose
    private Boolean isFavourite;

    public Product(){

    }
    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryDescAr() {
        return categoryDescAr;
    }

    public void setCategoryDescAr(String categoryDescAr) {
        this.categoryDescAr = categoryDescAr;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubCategoryDescAr() {
        return subCategoryDescAr;
    }

    public void setSubCategoryDescAr(String subCategoryDescAr) {
        this.subCategoryDescAr = subCategoryDescAr;
    }

    public String getSubCategoryDesc() {
        return subCategoryDesc;
    }

    public void setSubCategoryDesc(String subCategoryDesc) {
        this.subCategoryDesc = subCategoryDesc;
    }

    public Long getID() {
        return iD;
    }

    public void setID(Long iD) {
        this.iD = iD;
    }

    public String getItemNameAR() {
        return itemNameAR;
    }

    public void setItemNameAR(String itemNameAR) {
        this.itemNameAR = itemNameAR;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getImagesPath() {
        return imagesPath;
    }

    public void setImagesPath(String imagesPath) {
        this.imagesPath = imagesPath;
    }

    public String getItemSellerName() {
        return itemSellerName;
    }

    public void setItemSellerName(String itemSellerName) {
        this.itemSellerName = itemSellerName;
    }

    public Long getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Long itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Boolean getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

}
