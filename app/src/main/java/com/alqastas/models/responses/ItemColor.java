
package com.alqastas.models.responses;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemColor implements Serializable
{

    @SerializedName("Id")
    @Expose
    private Long id;
    @SerializedName("ItemId")
    @Expose
    private Long itemId;
    @SerializedName("ColorId")
    @Expose
    private Long colorId;
    @SerializedName("ColorDescAR")
    @Expose
    private String colorDescAR;
    @SerializedName("ColorDesc")
    @Expose
    private String colorDesc;
    private final static long serialVersionUID = -5459142403949341536L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getColorId() {
        return colorId;
    }

    public void setColorId(Long colorId) {
        this.colorId = colorId;
    }

    public String getColorDescAR() {
        return colorDescAR;
    }

    public void setColorDescAR(String colorDescAR) {
        this.colorDescAR = colorDescAR;
    }

    public String getColorDesc() {
        return colorDesc;
    }

    public void setColorDesc(String colorDesc) {
        this.colorDesc = colorDesc;
    }

}
