
package com.alqastas.models.responses;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemReview implements Serializable
{

    @SerializedName("ReviewsID")
    @Expose
    private Long reviewsID;
    @SerializedName("Userid")
    @Expose
    private Long userid;
    @SerializedName("ItemId")
    @Expose
    private Long itemId;
    @SerializedName("Rate")
    @Expose
    private Float rate;
    @SerializedName("Comments")
    @Expose
    private String comments;
    @SerializedName("ReviewerName")
    @Expose
    private String reviewerName;
    private final static long serialVersionUID = 3046509136369015667L;

    public Long getReviewsID() {
        return reviewsID;
    }

    public void setReviewsID(Long reviewsID) {
        this.reviewsID = reviewsID;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

}
