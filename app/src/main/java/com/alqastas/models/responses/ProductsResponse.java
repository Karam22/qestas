package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductsResponse extends DefaultResponse implements Serializable {

    @SerializedName("Products")
    private List<Product> productList;

    public List<Product> getProductList() {
        return productList;
    }
}
