package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CategoriesResponse extends DefaultResponse implements Serializable {

    @SerializedName("Categories")
    private List<Category> categoryList;

    public List<Category> getCategoryList() {
        return categoryList;
    }
}
