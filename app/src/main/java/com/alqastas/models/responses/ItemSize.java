
package com.alqastas.models.responses;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemSize implements Serializable
{

    @SerializedName("Id")
    @Expose
    private Long id;
    @SerializedName("ItemId")
    @Expose
    private Long itemId;
    @SerializedName("SizeId")
    @Expose
    private Long sizeId;
    @SerializedName("SizeDescAr")
    @Expose
    private String sizeDescAr;
    @SerializedName("SizeDesc")
    @Expose
    private String sizeDesc;
    private final static long serialVersionUID = 7599350004975407653L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getSizeId() {
        return sizeId;
    }

    public void setSizeId(Long sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeDescAr() {
        return sizeDescAr;
    }

    public void setSizeDescAr(String sizeDescAr) {
        this.sizeDescAr = sizeDescAr;
    }

    public String getSizeDesc() {
        return sizeDesc;
    }

    public void setSizeDesc(String sizeDesc) {
        this.sizeDesc = sizeDesc;
    }

}
