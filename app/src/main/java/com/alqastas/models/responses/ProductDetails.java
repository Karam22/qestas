
package com.alqastas.models.responses;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetails implements Serializable
{

    @SerializedName("Id")
    @Expose
    private Long id;
    @SerializedName("ItemNameAR")
    @Expose
    private String itemNameAR;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("ItemDescAr")
    @Expose
    private String itemDescAr;
    @SerializedName("ItemDesc")
    @Expose
    private String itemDesc;
    @SerializedName("SubCategoryId")
    @Expose
    private Long subCategoryId;
    @SerializedName("SubCategoryDesc")
    @Expose
    private String subCategoryDesc;
    @SerializedName("SubCategoryDescAR")
    @Expose
    private String subCategoryDescAR;
    @SerializedName("CategoryId")
    @Expose
    private Long categoryId;
    @SerializedName("MainCategoryDesc")
    @Expose
    private String mainCategoryDesc;
    @SerializedName("MainCategoryDescAR")
    @Expose
    private String mainCategoryDescAR;
    @SerializedName("ItemPrice")
    @Expose
    private Long itemPrice;
    @SerializedName("SellerId")
    @Expose
    private Long sellerId;
    @SerializedName("SellerName")
    @Expose
    private String sellerName;
    @SerializedName("IsActive")
    @Expose
    private Long isActive;
    @SerializedName("CreatedBy")
    @Expose
    private Long createdBy;
    @SerializedName("Rate")
    @Expose
    private Float rating;
    @SerializedName("InsuranceEndAt")
    @Expose
    private String InsuranceEndAt;
    @SerializedName("CreatedbyName")
    @Expose
    private String createdbyName;
    @SerializedName("CreatedAt")
    @Expose
    private String createdAt;
    @SerializedName("isFavourite")
    @Expose
    private Boolean isFavourite;
    @SerializedName("itemSizes")
    @Expose
    private List<ItemSize> itemSizes = null;
    @SerializedName("itemColors")
    @Expose
    private List<ItemColor> itemColors = null;
    @SerializedName("itemImages")
    @Expose
    private List<ItemImage> itemImages = null;
    @SerializedName("itemReviews")
    @Expose
    private List<ItemReview> itemReviews = null;
    private final static long serialVersionUID = 8978409462790290888L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemNameAR() {
        return itemNameAR;
    }

    public void setItemNameAR(String itemNameAR) {
        this.itemNameAR = itemNameAR;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescAr() {
        return itemDescAr;
    }

    public void setItemDescAr(String itemDescAr) {
        this.itemDescAr = itemDescAr;
    }

    public String getInsuranceEndAt() {
        return InsuranceEndAt;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubCategoryDesc() {
        return subCategoryDesc;
    }

    public void setSubCategoryDesc(String subCategoryDesc) {
        this.subCategoryDesc = subCategoryDesc;
    }

    public String getSubCategoryDescAR() {
        return subCategoryDescAR;
    }

    public void setSubCategoryDescAR(String subCategoryDescAR) {
        this.subCategoryDescAR = subCategoryDescAR;
    }

    public Float getRating() {
        return rating;
    }

    public Boolean getFavourite() {
        return isFavourite;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getMainCategoryDesc() {
        return mainCategoryDesc;
    }

    public void setMainCategoryDesc(String mainCategoryDesc) {
        this.mainCategoryDesc = mainCategoryDesc;
    }

    public String getMainCategoryDescAR() {
        return mainCategoryDescAR;
    }

    public void setMainCategoryDescAR(String mainCategoryDescAR) {
        this.mainCategoryDescAR = mainCategoryDescAR;
    }

    public Long getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Long itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedbyName() {
        return createdbyName;
    }

    public void setCreatedbyName(String createdbyName) {
        this.createdbyName = createdbyName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public List<ItemSize> getItemSizes() {
        return itemSizes;
    }

    public void setItemSizes(List<ItemSize> itemSizes) {
        this.itemSizes = itemSizes;
    }

    public List<ItemColor> getItemColors() {
        return itemColors;
    }

    public void setItemColors(List<ItemColor> itemColors) {
        this.itemColors = itemColors;
    }

    public List<ItemImage> getItemImages() {
        return itemImages;
    }

    public void setItemImages(List<ItemImage> itemImages) {
        this.itemImages = itemImages;
    }

    public List<ItemReview> getItemReviews() {
        return itemReviews;
    }

    public void setItemReviews(List<ItemReview> itemReviews) {
        this.itemReviews = itemReviews;
    }

}
