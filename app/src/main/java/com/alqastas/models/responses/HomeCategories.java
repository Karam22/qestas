
package com.alqastas.models.responses;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeCategories implements Serializable
{

    @SerializedName("CategoryId")
    @Expose
    private Long categoryId;
    @SerializedName("CategoryDescAr")
    @Expose
    private String categoryDescAr;
    @SerializedName("CategoryDesc")
    @Expose
    private String categoryDesc;
    @SerializedName("MainCategoryId")
    @Expose
    private Long mainCategoryId;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("ImagesPath")
    @Expose
    private String imagesPath;
    @SerializedName("Products")
    @Expose
    private List<Product> products = null;



    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryDescAr() {
        return categoryDescAr;
    }

    public void setCategoryDescAr(String categoryDescAr) {
        this.categoryDescAr = categoryDescAr;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    public Long getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(Long mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getImagesPath() {
        return imagesPath;
    }

    public void setImagesPath(String imagesPath) {
        this.imagesPath = imagesPath;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
