package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

public class UserResponse extends DefaultResponse {

    @SerializedName("access_token")
    private String token;

    @SerializedName("UserID")
    private String userId;

    @SerializedName("userName")
    private String userName;

    @SerializedName("DisplayName")
    private String displayName;

    public String getToken() {
        return token;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
