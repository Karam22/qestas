package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HomeResponse implements Serializable {

    @SerializedName("Categories")
    private List<HomeCategories> homeCategoriesList ;
    @SerializedName("Announcements")
    private List<AnnoncmentResponse> annoncmentResponseList ;

    public List<AnnoncmentResponse> getAnnoncmentResponseList() {
        return annoncmentResponseList;
    }

    public List<HomeCategories> getHomeCategoriesList() {
        return homeCategoriesList;
    }
}
