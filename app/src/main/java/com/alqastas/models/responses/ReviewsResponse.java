package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ReviewsResponse implements Serializable {

    @SerializedName("Reviews")
    private List<ItemReview> commentsList;

    public List<ItemReview> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<ItemReview> commentsList) {
        this.commentsList = commentsList;
    }
}
