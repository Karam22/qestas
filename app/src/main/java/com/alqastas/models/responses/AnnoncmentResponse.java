package com.alqastas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnnoncmentResponse {

    @SerializedName("AnnoncmentID")
    private long id;

    @SerializedName("AnnoncmentText")
    private String annoncmentText;
    @SerializedName("AnnoncmentImagePath")
    private String annoncmentImagePath;
    @SerializedName("Destntion")
    private String destination;
    @SerializedName("destintionType")
    private String destinationType;
    @SerializedName("ClickedCounter")
    private String clickedCounter;

    public long getId() {
        return id;
    }

    public String getAnnoncmentImagePath() {
        return annoncmentImagePath;
    }

    public String getAnnoncmentText() {
        return annoncmentText;
    }

    public String getClickedCounter() {
        return clickedCounter;
    }

    public String getDestination() {
        return destination;
    }

    public String getDestinationType() {
        return destinationType;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAnnoncmentImagePath(String annoncmentImagePath) {
        this.annoncmentImagePath = annoncmentImagePath;
    }

    public void setAnnoncmentText(String annoncmentText) {
        this.annoncmentText = annoncmentText;
    }

    public void setClickedCounter(String clickedCounter) {
        this.clickedCounter = clickedCounter;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setDestinationType(String destinationType) {
        this.destinationType = destinationType;
    }
}
