package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PromoCodeResponse implements Serializable {

    @SerializedName("Amount")
    private int amount;
    @SerializedName("IsActive")
    private boolean isActive;

    public int getAmount() {
        return amount;
    }

    public boolean isActive() {
        return isActive;
    }
}
