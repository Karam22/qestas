package com.alqastas.models.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubCategory implements Serializable {

    @SerializedName("id")
    private long id;
    @SerializedName("title")
    private String title;
    @SerializedName("subImage")
    private String image;

    private boolean isChecked  = false;
    public long getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
