
package com.alqastas.models.requests;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderRequest implements Serializable {

    @SerializedName("UserId")
    @Expose
    private Long userId;
    @SerializedName("PromoCode")
    @Expose
    private String promoCode;
    @SerializedName("UserAddressId")
    @Expose
    private Long userAddressId;
    @SerializedName("OrderDetails")
    @Expose
    private List<OrderDetail> orderDetails = null;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Long getUserAddressId() {
        return userAddressId;
    }

    public void setUserAddressId(Long userAddressId) {
        this.userAddressId = userAddressId;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

}
