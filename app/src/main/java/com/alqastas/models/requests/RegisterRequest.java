
package com.alqastas.models.requests;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterRequest implements Serializable
{

    @SerializedName("LoginName")
    @Expose
    private String loginName;
    @SerializedName("PWD")
    @Expose
    private String pWD;
    @SerializedName("Telephone")
    @Expose
    private String telephone;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("Lastname")
    @Expose
    private String lastname;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Licensesnumber")
    @Expose
    private String licensesnumber;
    @SerializedName("ImageURl")
    @Expose
    private String imageURl;
    @SerializedName("CreatedBy")
    @Expose
    private Long createdBy;
    @SerializedName("UserTypeId")
    @Expose
    private Long userTypeId;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPWD() {
        return pWD;
    }

    public void setPWD(String pWD) {
        this.pWD = pWD;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLicensesnumber() {
        return licensesnumber;
    }

    public void setLicensesnumber(String licensesnumber) {
        this.licensesnumber = licensesnumber;
    }

    public String getImageURl() {
        return imageURl;
    }

    public void setImageURl(String imageURl) {
        this.imageURl = imageURl;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Long userTypeId) {
        this.userTypeId = userTypeId;
    }

}
