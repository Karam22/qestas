package com.alqastas.models;

public class LoadingProductsListener {

    private long subCategoryId;

    public LoadingProductsListener(long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }
}
