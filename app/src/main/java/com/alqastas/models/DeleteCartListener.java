package com.alqastas.models;

public class DeleteCartListener {

    private long cartId;
    private  int position;

    public DeleteCartListener (long cartId , int position){
        this.cartId = cartId;
        this.position = position;
    }

    public long getCartId() {
        return cartId;
    }

    public int getPosition() {
        return position;
    }
}
