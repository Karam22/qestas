package com.alqastas.app;

import android.app.Application;

import com.alqastas.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AlQestasApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
